<!--
SPDX-FileCopyrightText: 2024 Karlsruhe Institute of Technology (KIT) <tomas.stary@kit.edu>

SPDX-License-Identifier: CC0-1.0
-->

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## Unreleased
### Added

## 0.0.0

### Added
- Add a docker-compose
- Rewrite dialysis history only related to optimisation (starting fresh from release 0.0.0)

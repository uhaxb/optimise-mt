#!/usr/bin/env python

# SPDX-FileCopyrightText: 2024 Karlsruhe Institute of Technology (KIT) <tomas.stary@kit.edu>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from carputils import tools
from random import randint

# list of mutations (as in matlab code)
mutations = ['CTRL', 'G480R', 'Y481H', 'G482Ra', 'G482Rb', 'A485V',
             'R524Q', 'K530N', 'D553N', 'S672R', 'E161K',
             'G1406R', 'dKPQ', 'E1784Ka', 'E1784Kb', 'D1790G',
             '1795insD', 'R231C', 'V241F', 'dF339']

# Create mutations parameter shifts table
PARAMETER_SHIFTS = {}

# the mutations are described in the Fabbri et al. 2017 paper
# from Table 2. 
# V_{1/2} (mV)
# mutation_index                   = [0, 1,   2,     3,      4,      5,     6,   7,     8,   9,    10,    11,   12,      13,     14,      15,     16,     17,18,    19]
PARAMETER_SHIFTS["y_inf_shift"]    = [0, -15, -43.9, -38.7,  0,      -30,   4.2, -14.2, 0,   -4.9, 0,     0,    0,       0,      0,       0,      0,      0, 0,     0]
# Tau shift (mV)
PARAMETER_SHIFTS["tau_y_shift"]    = [0, -15, -43.9, -38.7,  0,      -30,   4.2, -14.2, 0,   -4.9, 0,     0,    0,       0,      0,       0,      0,      0, 0,     0]
# current density (in percents)
PARAMETER_SHIFTS["g_f_increase"]   = [0, -50, 0,     0,      -65,    -66.4, 0,   0,     -63, 0,    0,     0,    0,       0,      0,       0,      0,      0, 0,     0]
# changes in I_{Na} model Table 3.
# activation V_{1/2} (mV)
PARAMETER_SHIFTS["m_inf_shift"]    = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    11.9,  0,    9,       8.8,    12.5,    6,      9.1,    0, 0,     0]
# activation slope (in percents)
PARAMETER_SHIFTS["m_inf_slope"]    = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    17.9,  0,    26.8,    78.1,   32.6,    33.3,   0,      0, 0,     0]
# activation Tau (mV)
PARAMETER_SHIFTS["m_tau_shift"]    = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    11.9,  0,    9,       8.8,    12.5,    6,      9.1,    0, 0,     0]
# inactivation V_{1/2} (mV)
PARAMETER_SHIFTS["h_inf_shift"]    = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    0,     0,    0,       -14.4,  -15,     -15,    -9.7,   0, 0,     0]
# inactivation slope (in percents)
PARAMETER_SHIFTS["h_inf_slope"]    = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    0,     0,    0,       0,      0,       -6.8,   0,      0, 0,     0]
# inactivation tau (multiplier??)
PARAMETER_SHIFTS["h_tau_gain"]     = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    0,     0,    0,       0,      0,       0.5,    0,      0, 0,     0]
# I_{Na} Late (in percents) of INa -- TODO: verify if the values are correct, for now it seams that they should be percentages of GNa = 0.0223 as in the comment behind the brackets
# recalculated as % of GNa
PARAMETER_SHIFTS["g_NaL"]          = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    0,     0,    0.00247, 0.0061, 0.00762, 0,      0.0059, 0, 0,     0] # [0.6% -> 0.00247];[1.5% -> 0.0061];[1.85% -> 0.00762];[1.4 -> 0.0059]
# recalculated by me (TS)
# PARAMETER_SHIFTS["g_NaL"]          = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    0,     0,   0.0001338,0.0003345,0.00041255,0,0.0003122,0,0,     0]
# current density (in percents)
PARAMETER_SHIFTS["g_Na_increase"]  = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    -60,   -100, 0,       0,      -40,     0,      -77.8,  0, 0,     0]
# changes in  I_{Ks} model Table 4.
# activation V_{1/2}
PARAMETER_SHIFTS["n_inf_shift"]    = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    0,     0,    0,       0,      0,       0,      0,      0, -43.2, 25]
# activation slope (in percents)
PARAMETER_SHIFTS["n_shift_slope"]  = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    0,     0,    0,       0,      0,       0,      0,      0, 52.9,  0]
# current density
PARAMETER_SHIFTS["g_Ks_increase"]  = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    0,     0,    0,       0,      0,       0,      0,      0, 0,     -69.5]
PARAMETER_SHIFTS["R231C"]          = [0, 0,   0,     0,      0,      0,     0,   0,     0,   0,    0,     0,    0,       0,      0,       0,      0,      1, 0,     0]

# Create CTRL Parameter Table (Iso and ACh Values to regulate CL)
PARAMETER_SHIFTS["Iso_1_uM"]       = [0, 0,   0,     0,      0,      1,     0,   0,     0,   0,    0,     0,    1,       0,      0,       0,      0,      0, 0,     0]
PARAMETER_SHIFTS["K_iso_shift"]    = [0, 0,   0,     0,      0,      0.04,  0,   0,     0,   0,    0,     0,    0.03,    0,      0,       0,      0,      0, 0,     0]
PARAMETER_SHIFTS["K_iso_increase"] = [0, 0,   0,     0,      0,      0.88,  0,   0,     0,   0,    0,     0,    0.88,    0,      0,       0,      0,      0, 0,     0]
PARAMETER_SHIFTS["K_iso_slope_dL"] = [0, 0,   0,     0,      0,      0.02,  0,   0,     0,   0,    0,     0,    0.012,   0,      0,       0,      0,      0, 0,     0]
PARAMETER_SHIFTS["ACh"]            = [0, 0,   0,     3.2e-6, 3.2e-6, 0,     0,   0,     0,   0,    18e-6, 0,    0,       0,      0,       1e-6,   0,      0, 0,     3.2e-6]

def output_mutation_alterations():
    for mutation in mutations:
        index = mutations.index(mutation)
        print("\n--------------------")
        print(index, ":", mutation)
        print("--------------------")
        for key in PARAMETER_SHIFTS.keys():
            value = PARAMETER_SHIFTS[key][index]
            if value:
                print(key, "=", value)


def parser():
    models = ['Loewe', 'Loewe_iso']
    parser = tools.standard_parser()
    group = parser.add_argument_group('Experiment specific options.')
    group.add_argument('--mutation',
                        default = mutations[0],
                        choices = mutations,
                        help = 'mutation variant')
    group.add_argument('--wt',
                       action='store_true',
                       help = 'switch for the model wild-type (WT), affects only Ach and Iso stimulation')
    group.add_argument('--imp',
                        default = models[0],
                        choices = models,
                        help = 'baseline model')
    group.add_argument('--iks',
                       type = int,
                       default = 0,
                       choices = [0, 1],
                       help = 'switch for IKs model between original (0) and Verkerk, Wilders model (1)')
    group.add_argument('--ical',
                       type = int,
                       default = 0,
                       choices = [0, 1],
                       help = 'switch for ICaL iso modulation between original (0) and split onto IsiK, IsiNa, IsiCa (1)')
    group.add_argument('--Iso_cas',
                       type = float,
                       default = 0.0,
                       help = 'switch for usage of isoprenaline cascade')
    group.add_argument('--Cao',
                       type = float,
                       default = 1.8,
                       help = 'switch to change extracellular Ca2+ concentration')
    group.add_argument('--linear-iso',
                       default = 0.0,
                       type = float,
                       help = 'Isoprenaline concentration for linear model')
    group.add_argument('--ach',
                        type = int,
                        default = 0,
                        choices = [0, 1],
                        help = 'switch for ACh modulation')
    group.add_argument('--duration', default=10000, type=float,
                        help = 'duration of the experiment (ms)')
    group.add_argument('--output', default='Trace_0.dat',
                        help = 'output filename')
    group.add_argument('--original-fabbri',
                       action='store_true',
                       default=False,
                       help = 'switch to Fabbri et al. 2017')
    group.add_argument('--parameters',
                       help = 'parameter values for optimisation')
    # TODO: this does not check if any of the parameters were altered for instance from original-fabbri or mutaitons!
    group.add_argument('--imp-par',
                       default="",
                       help = 'parameters to modify IMP, Warning: use with caution, might be incompatible with mutations or --original-fabbri switch')
    group.add_argument('--dt',
                       default=0.01,
                       help = 'time step [ms]')
    group.add_argument('--dt-out',
                       default=1.0,
                       help = 'temporal output granularity [ms]')
    group.add_argument('--start-out',
                       default=0.0,
                       help= 'start time of output [ms]')
    group.add_argument('--stim-cur',
                       default=0.0,
                       help='stimulation current [pA/pF]=[uA/cm^2]')
    group.add_argument('--save-ini-file',
                       default='singlecell.sv',
                       help='text file in which to save state of a single cell')
    group.add_argument('--read-ini-file',
                       default='singlecell.sv',
                       help='text file from which to read state of a single cell')
    group.add_argument('--save-ini-time',
                       default=0.0,
                       help='time at which to save single cell state [ms]')


    return parser


# define job ID
def jobID(args):
    """
    Generate name of top level output directory.
    """
    tpl = '{}_{}_for{}ms'
    return tpl.format(args.imp, args.mutation, args.duration)

def par_mutation(i_mut):
    imp_par= ""
    y_inf_shift = PARAMETER_SHIFTS["y_inf_shift"][i_mut]
    if y_inf_shift:
        imp_par += ',y_shift={}'.format(y_inf_shift)

    tau_y_shift = PARAMETER_SHIFTS["tau_y_shift"][i_mut]
    if tau_y_shift:
        imp_par += ',tau_y_a_shift={},tau_y_b_shift={}'.format(tau_y_shift, tau_y_shift)

    g_f_increase = PARAMETER_SHIFTS["g_f_increase"][i_mut]
    if g_f_increase:
        imp_par += ',Gf_K{}%,Gf_Na{}%'.format(g_f_increase, g_f_increase)

    m_inf_shift = PARAMETER_SHIFTS["m_inf_shift"][i_mut]
    if m_inf_shift:
        imp_par += ',m_inf_shift={}'.format(m_inf_shift)

    m_inf_slope = PARAMETER_SHIFTS["m_inf_slope"][i_mut]
    if m_inf_slope:
        imp_par += ',m_inf_slope+{}%'.format(m_inf_slope)

    m_tau_shift = PARAMETER_SHIFTS["m_tau_shift"][i_mut]
    if m_tau_shift:
        imp_par += ',m_tau_shift={}'.format(m_tau_shift)

    h_inf_shift = PARAMETER_SHIFTS["h_inf_shift"][i_mut]
    if h_inf_shift:
        imp_par += ',h_inf_shift={}'.format(h_inf_shift)

    h_inf_slope = PARAMETER_SHIFTS["h_inf_slope"][i_mut]
    if h_inf_slope:
        imp_par += ',h_inf_slope+{}%'.format(h_inf_slope)

    h_tau_gain = PARAMETER_SHIFTS["h_tau_gain"][i_mut]
    if h_tau_gain:
        imp_par += ',h_tau_gain*{}'.format(h_tau_gain)

    g_NaL = PARAMETER_SHIFTS["g_NaL"][i_mut]
    if g_NaL:
        imp_par += ',ratio_INaL_INa={}'.format(g_NaL)

    g_Na_increase = PARAMETER_SHIFTS["g_Na_increase"][i_mut]
    if g_Na_increase:
        imp_par += ',GNa{}%'.format(g_Na_increase)

    n_inf_shift = PARAMETER_SHIFTS["n_inf_shift"][i_mut]
    if n_inf_shift:
        imp_par += ',n_inf_shift={}'.format(n_inf_shift)

    n_shift_slope = PARAMETER_SHIFTS["n_shift_slope"][i_mut]
    if n_shift_slope:
        imp_par += ',n_shift_slope+{}%'.format(n_shift_slope)

    g_Ks_increase = PARAMETER_SHIFTS["g_Ks_increase"][i_mut]
    if g_Ks_increase:
        imp_par += ',GKs_{}%'.format(g_Ks_increase)

    R231C = PARAMETER_SHIFTS["R231C"][i_mut]
    if R231C:
        imp_par += ',R231C_on={}'.format(R231C)

    Iso_1_uM = PARAMETER_SHIFTS["Iso_1_uM"][i_mut]
    if Iso_1_uM:
        imp_par += ',Iso_1_uM={}'.format(Iso_1_uM)
        imp_par += ',Iso_linear=1'

    K_iso_shift = PARAMETER_SHIFTS["K_iso_shift"][i_mut]
    if K_iso_shift:
        imp_par += ',K_iso_shift={}'.format(K_iso_shift)

    K_iso_increase = PARAMETER_SHIFTS["K_iso_increase"][i_mut]
    if K_iso_increase:
        imp_par += ',K_iso_increase={}'.format(K_iso_increase)

    K_iso_slope_dL = PARAMETER_SHIFTS["K_iso_slope_dL"][i_mut]
    if K_iso_slope_dL:
        imp_par += ',K_iso_slope_dL={}'.format(K_iso_slope_dL)

    ACh = PARAMETER_SHIFTS["ACh"][i_mut]
    if ACh:
        imp_par += ',ACh={},ACh_on=1.0'.format(ACh)

    return imp_par


def assign_imp_par(P):
    imp_par = ''
    for key, value in P.items():
        imp_par += f',{key}={value}'
    return imp_par

def parse_parameters(parameters):
    params = parameters.replace('_',' ').split()
    assert(len(params) == 19)

    P = dict()
    P['INaK_max'] = float(params[0])
    P['P_CaL']    = float(params[1])
    P['k_dL']     = float(params[2])
    P['V_dL']     = float(params[3])
    P['k_fL']     = float(params[4])
    P['V_fL']     = float(params[5])
    P['GKur']     = float(params[6])
    P['Gto']      = float(params[7])
    P['GKr_']     = float(params[8])
    P['GKs_']     = float(params[9])
    P['Gf_K']     = float(params[10])
    P['Gf_Na']    = float(params[11])
    P['EC50_SK']  = float(params[12])
    P['n_SK']     = float(params[13])
    P['GSK']      = float(params[14])
    P['dynamic_Ki_Nai']      = float(params[15])
    P['P_CaT'] = float(params[16])
    P['K_NaCa']	 = float(params[17])  
    P['GKACh'] = float(params[18])
    
    return P


def print_tableI(P):
    print(f'''
---------------|--------------
Parameter      |  Model
---------------|--------------
i NaK max (nA) | {P['INaK_max']}
P CaL (nA/mM)  | {P['P_CaL']   }
k dl (mV)      | {P['k_dL']    }
V dl (mV)      | {P['V_dL']    }
k fl (mV)      | {P['k_fL']    }
V fl (mV)      | {P['V_fL']    }
g Kur (μS)     | {P['GKur']    }
g to (μS)      | {P['Gto']     }
g Kr (μS)      | {P['GKr_']    }
g Ks (μS)      | {P['GKs_']    }
g fK (μS)      | {P['Gf_K']    }
g fNa (μS)     | {P['Gf_Na']   }
---------------|---------------
EC50 SK (mM)   | {P['EC50_SK'] }
n SK           | {P['n_SK']    }
g SK (μS)      | {P['GSK']     }
---------------|---------------
dynamic_Ki_Nai | {P['dynamic_Ki_Nai']}
compare Loewe 2019 IEEE Tab. I
''')

def get_optimized_parameters(Loewe=True):
    if Loewe:
        # copied from Loewe.model
        # i NaK max (nA) 
        INaK_max = 0.137171
        # P CaL (nA/mM) 
        P_CaL    = 0.5046812
        # k dl (mV)
        k_dL     = 5.854
        # V dl (mV)
        V_dL     = (-7.7713)
        # k fl (mV)
        k_fL     = 5.1737
        # V fl (mV)
        V_fL     = -12.1847
        # g Kur (μS)
        GKur     = 7.062e-05
        # g to (μS)
        Gto      =  1.67347999999999998e-03
        # g Kr (μS)
        GKr_     =  4.98909900000000031e-03
        # g Ks (μS)
        GKs_     =  8.63714000000000018e-04
        # g fK (μS)
        Gf_K     = 0.0117
        # g fNa (μS) 
        Gf_Na    = 0.00696
        # EC50 SK (mM) 
        EC50_SK  = 0.7
        # n SK
        n_SK     = 2.2
        # g SK (μS)
        GSK      = 0.000165

        dynamic_Ki_Nai = 1
        
        # P CaT (nA/mM)
        P_CaT = 0.04132
        # K NaCa (nA)
        K_NaCa	 = 3.343
               
        GKACh = 0.00345

    else:
        # copied from FabbriEtAlInit0.ev file for acCELLerate
        # i NaK max (nA) 
        i_NaK_max  = 0.08105 
        # P CaL (nA/mM) 
        P_CaL      = 0.4578 
        # k dl (mV)
        k_dl       = 4.337 
        # V dl (mV)
        V_dl       = -16.45085327 
        # k fl (mV)
        k_fl       = 5.3 
        # V fl (mV)
        V_fl       = -37.4 
        # g Kur (μS)
        g_Kur      = 1.539000e-04 
        # g to (μS)
        g_to       = 0.0035 
        # g Kr (μS)
        g_Kr       = 0.00424 
        # g Ks (μS)
        g_Ks       = 6.500000e-04 
        # g fK (μS)
        g_fK       = 0.00268 
        # g fNa (μS) 
        g_fNa      = 0.00159 
        # EC50 SK (mM) 
        EC50_SK    = 0.7 
        # n SK
        n_SK       = 2.2 
        # g SK (μS)
        g_SK       = 0 

        dynamic_Ki_Nai = 0
        
        # P CaT (nA/mM)
        P_CaT = 0.04132
        # K NaCa (nA)
        K_NaCa	 = 3.343

        GKACh = 0.00345
        
        INaK_max = i_NaK_max
        P_CaL    = P_CaL
        k_dL     = k_dl
        V_dL     = V_dl
        k_fL     = k_fl
        V_fL     = V_fl
        GKur     = g_Kur
        Gto      = g_to
        GKr_     = g_Kr
        GKs_     = g_Ks
        Gf_K     = g_fK
        Gf_Na    = g_fNa
        EC50_SK  = EC50_SK
        n_SK     = n_SK
        GSK      = g_SK


    P = dict()
    P['INaK_max'] = INaK_max
    P['P_CaL']    = P_CaL
    P['k_dL']     = k_dL
    P['V_dL']     = V_dL
    P['k_fL']     = k_fL
    P['V_fL']     = V_fL
    P['GKur']     = GKur
    P['Gto']      = Gto
    P['GKr_']     = GKr_
    P['GKs_']     = GKs_
    P['Gf_K']     = Gf_K
    P['Gf_Na']    = Gf_Na
    P['EC50_SK']  = EC50_SK
    P['n_SK']     = n_SK
    P['GSK']      = GSK
    P['dynamic_Ki_Nai']      = dynamic_Ki_Nai
    
    P['P_CaT'] = P_CaT
    P['K_NaCa']	 = K_NaCa

    P['GKACh'] = GKACh


    return P


@tools.carpexample(parser, jobID, clean_pattern='exp*')
def run(args, job):

    cmd  = []
    # simulation settings
    cmd += ['--imp', args.imp]
    cmd += ['--duration', args.duration]
    # cmd += ['--numstim', 0]

    # choice of iks model
    imp_par ='VW_IKs={}'.format(args.iks)
    
    #choice of ICaL iso effect on split currents
    imp_par +=',ical_iso_split={}'.format(args.ical)
    
    #choice of ACh effect 
    # concentration of ACh is hardcoded here
    if (args.ach == 1):
    	imp_par +=',ACh=1e-05'


    i_mut = mutations.index(args.mutation)
    print("mutation", mutations[i_mut], "with index", i_mut)

    imp_par += par_mutation(i_mut)

    if args.parameters:
        P = parse_parameters(args.parameters)
        imp_par += assign_imp_par(P)

    if args.original_fabbri:
        P = get_optimized_parameters(False)
        print_tableI(P)
        imp_par += assign_imp_par(P)

	### adapt folders 
    if args.imp == 'Loewe' and args.original_fabbri:
        cmd += ['--read-ini-file', 'Init0.sv']
    elif args.imp == 'Loewe_iso' and args.original_fabbri:
        cmd += ['--read-ini-file', 'Init_fabbri_iso.sv']
    # Isoprenaline linear experiments
    if args.linear_iso:
        print("Simulating linear isoprenaline stimulation")
        if args.mutation != "CTRL":
            print("Warning: --linear-iso is tested only with the CTRL mutation (WT)")
        imp_par += ',Iso_1_uM={},Iso_linear={},K_iso_shift={},K_iso_increase={},K_iso_slope_dL={},K_iso_shift_ninf={},Cao={},Iso_cas={}'.format(
            1.0, 1.0, args.linear_iso, args.linear_iso, args.linear_iso, args.linear_iso, args.Cao, args.Iso_cas)
        print(imp_par)

    # cmd += ['--fout', 'tmp/Loewe']

    # numerical settings
    cmd += ['--dt', args.dt]
    cmd += ['--dt-out', args.dt_out]
    cmd += ['--start-out', args.start_out]
    cmd += ['--stim-cur', args.stim_cur]
    if args.imp_par:
        if args.mutation != "CTRL" or args.original_fabbri:
            print("Warning: using --imp-par might be incompatible with mutatations or --original-fabbri, use with caution!")
        imp_par += args.imp_par
    cmd += ['--imp-par', imp_par]

    # output settings
    # ouptut to Trace_$(trace_no).dat to avoid collisions
    trace_no = randint(0, 1e6)
    cmd += ['--trace-no', trace_no]
    if args.read_ini_file and not (args.read_ini_file == 'singlecell.sv'):
        cmd += ['--read-ini-file', args.read_ini_file]
    if args.save_ini_file and not (args.save_ini_file == 'singlecell.sv'):
        cmd += ['--save-ini-file', args.save_ini_file]
    if args.save_ini_time and not (args.save_ini_time == 0.0):
        cmd += ['--save-ini-time', args.save_ini_time]
    

    # cmd += ['--fout', os.path.join('./tmp/', job.ID, args.mutation)]
    # cmd += ['--validate']

    # start the cellular simulation
    title = 'Simulating cellular model {} with mutation {} for {} ms'
    job.bench(cmd, msg=title.format(args.imp, args.mutation, args.duration))
    job.mv(os.path.join('Trace_{}.dat'.format(trace_no)), os.path.join(args.output))

if __name__ == '__main__':
    run()
    # output_mutation_alterations()

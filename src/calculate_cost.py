import os
import matplotlib.pyplot as plt

sd_factor = 0.4 # 40% used for calcium transient markers
experimental_targets = [16.4, 828, -61.7, 143.5, 101.5, 64.9, 4.6, 48.9, 220, 105, 394, 217.4, 138.9, 115, 140, 5.0] # Verkerk 2007/2013 values. Fabbri standard concentrations for K_i/Na_i
weights = [1.0, 0.0, 1.0, 0.33, 0.33, 0.33, 1.0, 1.0, 0.1, 0.1, 0.16, 0.16, 0.16, 0.3, 0.5, 0.5]
experimental_sd = [0.9, 21, 6.1, 49.3, 38.2, 23.9, 1.7, 25.5, 
				sd_factor*220, sd_factor*105, sd_factor*394.0, sd_factor*217.4, sd_factor*138.9, sd_factor*115, sd_factor*140, sd_factor*5] # SD from Verkerk 2007/2013
result_model_markers = [[17.6, 830, -62.38, 110.0, 91.0, 61.0, 7.9, 41.9, 333.75, 130.86, 459.0, 134.0, 99.0, 202.8, 139.3, 5.9], #A
						[23.75, 831, -57.54, 145.0, 117.0, 73.0, 8.5, 60.5, 299.33, 120.48, 501.0, 168.0, 120.0, 178.85, 138.8, 6.4], #B
						[19.55, 829, -59.61, 131.0, 106.0, 71.0, 7.11, 24.05, 259.66, 106.59, 508.0, 170.0, 116.0, 153.06, 139.26, 5.97], #C 
						[17.24,	828,-58.12,	122.0,	98.0,	64.0, 6.28, 86.9, 169.19, 78.60, 552.0, 184.0, 118.0, 90.59, 140.08, 5.19], #D 
						]
labels = [
		'vm_overshoot', 'CL', 'vm_min', 'APD90', 'APD50', 'APD20',
			'Maxdvdt', 'DDR100', 'ca_overshoot', 'ca_min', 'TD90',
			'TD50', 'TD20', 'TA', 'ki_overshoot', 'nai_overshoot']
				
def cost_fabbri_sd(simulation, target, sd, weight):
	diff = abs(simulation - target)
	if diff > sd:
		return weight*((diff-sd)/sd)
	return 0
	
for j in range(len(result_model_markers)):
	cost_vector = []
	costs = 0
	for i in range(len(result_model_markers[0])):
		cost_vector.append(cost_fabbri_sd(result_model_markers[j][i], experimental_targets[i], experimental_sd[i], weights[i]))
		costs += cost_vector[i]
	print(costs)
	print(labels)
	print(cost_vector)
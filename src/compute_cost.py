# SPDX-FileCopyrightText: 2024 Karlsruhe Institute of Technology (KIT) <tomas.stary@kit.edu>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import fileinput

from optimisation_parallel import get_opt_args, weighted_rms, cost_lutz, extract_simulation_metrics
# from compute_metrics import metrics_keys

def print_cost(header,simulation, *args):
    target     = args[0]
    SD         = args[1]
    weights    = args[2]

    cost = 0
    for key in header:
        curr_cost = cost_lutz(simulation[key], target[key], SD[key], weights[key])
        print(curr_cost, end='\t')
        cost += curr_cost
    print()

def run():
    opt_args = get_opt_args()
    with fileinput.input() as f:
        header = f.readline().rstrip().split()
        print(*header, sep='\t')
        for line in f:
            markers = [float(i) for i in line.rstrip().split()]
            metrics = extract_simulation_metrics(header, markers)
            print_cost(header, metrics, *opt_args)

if __name__ == '__main__':
    run()

import subprocess
from math import log10, floor
import os 
import numpy as np

def generate_variations(base_value, variation, length, mode='add'):
    """
    Generate an array of variations of a floating-point value.
    
    Parameters:
        base_value (float): The starting floating-point value.
        variation (float): The value to add, subtract, or multiply with.
        length (int): The length of the resulting array.
        mode (str): 'add' for adding/subtracting, 'mult' for multiplying/dividing.
        
    Returns:
        list: An array of variations.
    """
    if length <= 0:
        raise ValueError("Length of the array must be greater than 0.")
    if mode not in ['add', 'mult']:
        raise ValueError("Mode must be either 'add' or 'mult'.")

    #variations = [base_value]
    variations = []
    if mode == 'add':
        # Generate variations by adding and subtracting the variation value
        for i in range(length):
            if i % 2 == 0:
                variations.append(base_value + (variation * (i // 2 + 1)))
            else:
                variations.append(base_value - (variation * (i // 2 + 1)))
    elif mode == 'mult':
        # Generate variations by multiplying and dividing by the variation value
        for i in range(length):
            if i % 2 == 0:
                variations.append(base_value * (variation ** (i // 2 + 1)))
            else:
                variations.append(base_value / (variation ** (i // 2 + 1)))

    return variations

def execute(cmd):
	popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
	for stdout_line in iter(popen.stdout.readline, ""):
		yield stdout_line
	popen.stdout.close()
	return_code = popen.wait()
	
def truncate_float(float_number, decimal_places):
	magnitude = floor(log10(abs(float_number)))
	multiplier = 10 ** (decimal_places - magnitude - 1)
    #if float_number > 1e5:
    #	return int(float_number)
	return int(float_number * multiplier) / multiplier

def run():	
	initial = dict()
	initial['INaK_max']		 = 0.09375326
	initial['P_CaL']	 = 0.55651542
	initial['k_dL']	 = 4.56004297
	initial['V_dL']		 = -18.51917308
	initial['k_fL']		 = 8.57249095
	initial['V_fL']		   = -45.57529252
	initial['GKur']			= 0.00012179 
	initial['Gto']	 = 0.00178041
	initial['GKr_']	 = 0.00485264	
	initial['GKs_']		 = 0.00096046 
	initial['Gf_K']		 = 0.00188198
	initial['Gf_Na'] = 0.00192289
	initial['EC50_SK']		  = 0.7 
	initial['n_SK']			  = 2.2 
	initial['GSK']	 = 0.00064602
	initial['dynamic_Ki_Nai'] = 1 
	initial['P_CaT']	 = 0.02734572 
	initial['K_NaCa']			  = 4.13200583
	initial['K_iso_shift'] = 1
	initial['K_iso_shift_ninf'] = 1
	initial['K_iso_increase'] = 1 #1
	initial['K_iso_slope_dL'] = 1
	#initial['K_up']		 = 286.1 # same in Loewe
	#initial['ks']		 = 1.480e8 # same in Loewe
	#initial['Ca_intracellular_fluxes_tau_dif_Ca'] = 5.469e-5 # same in Loewe
	#initial['kf_CM']	 = 1.642e6 # same in Loewe
	#initial['kf_CQ']	 = 175.4 # same in Loewe
	
	variation = dict()
	variation['INaK_max']		 = 0.09375326
	variation['INaK_max'] = generate_variations(initial['INaK_max'], 2.0, 6, 'mult')
	
	variation['P_CaL']	 = 0.55651542
	variation['k_dL']	 = 4.56004297
	variation['V_dL']		 = -18.51917308
	variation['k_fL']		 = 8.57249095
	variation['V_fL']		   = -45.57529252
	variation['GKur']			= 0.00012179 
	variation['GKur'] = generate_variations(initial['GKur'], 2.0, 6, 'mult')
	
	variation['Gto']	 = 0.00178041
	variation['Gto'] = generate_variations(initial['Gto'], 2.0, 6, 'mult')
	
	variation['GKr_']	 = 0.00485264	
	variation['GKr_'] = generate_variations(initial['GKr_'], 2.0, 6, 'mult')
	
	variation['GKs_']		 = 0.00096046 
	variation['GKs_'] = generate_variations(initial['GKs_'], 2.0, 6, 'mult')
	
	variation['Gf_K']		 = 0.00188198
	variation['Gf_K'] = generate_variations(initial['Gf_K'], 2.0, 6, 'mult')

	variation['Gf_Na'] = 0.00192289
	variation['Gf_Na'] = generate_variations(initial['Gf_Na'], 2.0, 6, 'mult')
	
	variation['EC50_SK']		  = 0.7 
	variation['n_SK']			  = 2.2 
	variation['GSK']	 = 0.00064602
	variation['dynamic_Ki_Nai'] = 1 
	variation['P_CaT']	 = 0.02734572 
	variation['K_NaCa']			  = 4.13200583
	variation['K_iso_shift'] = 1
	variation['K_iso_shift_ninf'] = 1
	variation['K_iso_increase'] = 1 #1
	variation['K_iso_slope_dL'] = 1
	#variation['K_up']		 = 286.1 # same in Loewe
	#variation['ks']		 = 1.480e8 # same in Loewe
	#variation['Ca_intracellular_fluxes_tau_dif_Ca'] = 5.469e-5 # same in Loewe
	#variation['kf_CM']	 = 1.642e6 # same in Loewe
	#variation['kf_CQ']	 = 175.4 # same in Loewe

	labels = ['INaK_max', 'P_CaL', 'k_dL', 'V_dL', 'k_fL', 'V_fL', 'GKur',
				  'Gto', 'GKr_', 'GKs_', 'Gf_K', 'Gf_Na', 'EC50_SK', 'n_SK', 
				  'GSK', 'dynamic_Ki_Nai', 'P_CaT', 'K_NaCa', 
				  'K_iso_shift', 'K_iso_shift_ninf', 'K_iso_increase', 'K_iso_slope_dL'] 
	#			  'K_up', 'ks', 'Ca_intracellular_fluxes_tau_dif_Ca', 'kf_CM', 'kf_CQ']

	#varied_parameter = ['INaK_max', "P_CaL"]
	varied_parameter = ['INaK_max', 'GKur', 'Gto', 'GKr_', 'GKs_', 'Gf_K', 'Gf_Na']	
	combined_varied_parameters = []
	
	if not varied_parameter == []:  	
		filenames = []	
		filenames_iso = []	
		i = 0
		j = 0
		k = 0
		for k in range(len(varied_parameter)):
			for j in range(len(variation[varied_parameter[k]])):
				parameters = ""
				for label in labels:
					value = truncate_float(initial[label], 5)
					if (label == varied_parameter[k]):
						value = truncate_float(variation[label][j], 5)
					parameters += f"{value}_"
				filename = "dat/metricsLoewe_" + parameters + "dur100000ms.dat"
				filenames.append(filename)
				filename_iso = "dat/iso_metricsLoewe_" + parameters + "dur100000ms.dat"
				filenames_iso.append(filename_iso)
			
		for i in range(len(filenames)):
			command = ['make', filenames[i]]
			command_iso = ['make', '-f', 'Makefile_iso', filenames_iso[i]]
			for line in execute(command):
				print(line, end='')
			for line in execute(command_iso):
				print(line, end='')
			
		j = 0
		k = 0
		for k in range(len(varied_parameter)):
			mypath = "dat/" + varied_parameter[k] + "/"
			if not os.path.isdir(mypath):
				os.makedirs(mypath)
			for j in range(len(variation[varied_parameter[k]])):
				parameters = ""
				for label in labels:
					value = truncate_float(initial[label], 5)
					if (label == varied_parameter[k]):
						value = truncate_float(variation[label][j], 5)
					parameters += f"{value}_"
				filename = "dat/metricsLoewe_" + parameters + "dur100000ms.dat"
				filename_iso = "dat/iso_metricsLoewe_" + parameters + "dur100000ms.dat"
				os.rename(filename, "dat/" + varied_parameter[k] + "/Loewemetrics_" + varied_parameter[k] + "_" + str(truncate_float(variation[varied_parameter[k]][j], 5)) + "dur.dat")
				os.rename(filename_iso, "dat/" + varied_parameter[k] + "/Loeweiso_metrics_" + varied_parameter[k] + "_" + str(truncate_float(variation[varied_parameter[k]][j], 5)) + "dur.dat")
				filename = "dat/Loewe_" + parameters + "dur100000ms.dat"
				filename_iso = "dat/iso_Loewe_" + parameters + "dur100000ms.dat"
				os.rename(filename, "dat/" + varied_parameter[k] + "/Loewe" + varied_parameter[k] + "_" + str(truncate_float(variation[varied_parameter[k]][j], 5)) + "dur.dat")
				os.rename(filename_iso, "dat/" + varied_parameter[k] + "/Loeweiso_" + varied_parameter[k] + "_" + str(truncate_float(variation[varied_parameter[k]][j], 5)) + "dur.dat")
				
	if not combined_varied_parameters == []:  	
		filenames = []	
		filenames_iso = []	
		i = 0
		j = 0
		k = 0
		for k in range(len(varied_parameter)):
			for j in range(len(variation[varied_parameter[k]])):
				parameters = ""
				for label in labels:
					value = truncate_float(initial[label], 5)
					if (label == varied_parameter[k]):
						value = truncate_float(variation[label][j], 5)
					parameters += f"{value}_"
				filename = "dat/metricsLoewe_" + parameters + "dur100000ms.dat"
				filenames.append(filename)
				filename_iso = "dat/iso_metricsLoewe_" + parameters + "dur100000ms.dat"
				filenames_iso.append(filename_iso)
			
		for i in range(len(filenames)):
			command = ['make', filenames[i]]
			command_iso = ['make', '-f', 'Makefile_iso', filenames_iso[i]]
			for line in execute(command):
				print(line, end='')
			for line in execute(command_iso):
				print(line, end='')
			
		j = 0
		k = 0
		for k in range(len(varied_parameter)):
			mypath = "dat/" + varied_parameter[k] + "/"
			if not os.path.isdir(mypath):
				os.makedirs(mypath)
			for j in range(len(variation[varied_parameter[k]])):
				parameters = ""
				for label in labels:
					value = truncate_float(initial[label], 5)
					if (label == varied_parameter[k]):
						value = truncate_float(variation[label][j], 5)
					parameters += f"{value}_"
				filename = "dat/metricsLoewe_" + parameters + "dur100000ms.dat"
				filename_iso = "dat/iso_metricsLoewe_" + parameters + "dur100000ms.dat"
				os.rename(filename, "dat/" + varied_parameter[k] + "/Loewemetrics_" + varied_parameter[k] + "_" + str(truncate_float(variation[varied_parameter[k]][j], 5)) + "dur.dat")
				os.rename(filename_iso, "dat/" + varied_parameter[k] + "/Loeweiso_metrics_" + varied_parameter[k] + "_" + str(truncate_float(variation[varied_parameter[k]][j], 5)) + "dur.dat")
				filename = "dat/Loewe_" + parameters + "dur100000ms.dat"
				filename_iso = "dat/iso_Loewe_" + parameters + "dur100000ms.dat"
				os.rename(filename, "dat/" + varied_parameter[k] + "/Loewe" + varied_parameter[k] + "_" + str(truncate_float(variation[varied_parameter[k]][j], 5)) + "dur.dat")
				os.rename(filename_iso, "dat/" + varied_parameter[k] + "/Loeweiso_" + varied_parameter[k] + "_" + str(truncate_float(variation[varied_parameter[k]][j], 5)) + "dur.dat")
	
if __name__ == '__main__':
	run()

	 


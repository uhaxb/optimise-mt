# SPDX-FileCopyrightText: 2024 Karlsruhe Institute of Technology (KIT) <tomas.stary@kit.edu>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import numpy as np
import sys
from scipy.signal import find_peaks
import statistics
from typing import Tuple, List, Any

# columns of printed markers
file_header = ['vm_overshoot', 'CL', 'vm_min', 'APD90', 'APD50', 'APD20', 'Maxdvdt', 'DDR100', 'vm_amplitude', 
               'ca_overshoot', 'CL_Ca', 'ca_min', 'TD90', 'TD50', 'TD20', 'MaxdCadt', 'DDR100_Ca', 'TA',
               'ki_overshoot', 'CL_Ki', 'ki_min', 'TD90_ki', 'TD50_ki', 'TD20_ki', 'MaxdKidt', 'DDR100_Ki', 'ki_amplitude',
               'nai_overshoot', 'CL_Nai', 'nai_min', 'TD90_nai', 'TD50_nai', 'TD20_nai', 'MaxdNaidt', 'DDR100_Nai', 'nai_amplitude']
metrics_keys = ['vm_overshoot', 'CL', 'vm_min', 'APD90', 'APD50', 'APD20', 'Maxdvdt', 'DDR100', 'ca_overshoot', 
                'ca_min', 'TD90', 'TD50', 'TD20', 'TA', 'ki_overshoot', 'nai_overshoot']


def extract_metrics(markers: Tuple) -> List:
    """Extraction of metrics

    Args:
        markers (Tuple): contains markers for Vm and Cai

    Returns:
        List: contains metrics in reference to metrics_keys
    """
    metrics = []
    for key in metrics_keys:
        index = file_header.index(key)
        metrics.append(markers[index])
    return metrics


def diastolic_depolarization_rate(time: np.ndarray[Any, Any], vm: np.ndarray[Any, Any]) -> Any:
    """Calculation of Diastolic Depolarisation

    Args:
        time (np.ndarray[Any, Any]): time
        vm (np.ndarray[Any, Any]): 1AP membrane potential in reference to time

    Returns:
        Any: DDR 100 in mV/s
    """
    index_diastole = 0
    for index in range(1,len(time)):
        diff_time = time[index] - time[index_diastole]
        if diff_time > 100:
            return (vm[index] - vm[index_diastole])/diff_time/1e-3


def action_potential_duration(time: np.ndarray[Any, Any], vm: np.ndarray[Any, Any]) -> Any:
    """_summary_

    Args:
        time (np.ndarray[Any, Any]): time
        vm (np.ndarray[Any, Any]): 1AP membrane potential in reference to time

    Returns:
        Any: APD for 90, 50 and 20% repolarisation

    Note: can be abused for other transient durations (such as cai)
    """
    dv = np.gradient(vm)
    dt = np.gradient(time)
    dvdt = np.divide(dv, dt)

    index_maxdvdt = dvdt.argmax()
    index_maxvm = vm.argmax()

    amplitude_vm = vm.max() - vm.min()
    
    APD90 = 0
    APD50 = 0
    APD20 = 0
    for i in range(len(time[index_maxvm:])):
        index = index_maxvm + i
        depolarization = vm[index_maxvm] - vm[index]
        if not APD90 and depolarization > 0.9*amplitude_vm:
            APD90 = time[index] - time[index_maxdvdt]
        if not APD50 and depolarization > 0.5*amplitude_vm:
            APD50 = time[index] - time[index_maxdvdt]
        if not APD20 and depolarization > 0.2*amplitude_vm:
            APD20 = time[index] - time[index_maxdvdt]

    return APD90, APD50, APD20


def markers(time: np.ndarray[Any, Any], trace: np.ndarray[Any, Any]) -> Any:
    """Computation of AP characteristics

    Args:
        time (np.ndarray[Any, Any]): time
        trace (np.ndarray[Any, Any]): values in reference to time

    Returns:
        Any: AP characteristics
    """
    CL = time[-1] - time[0]
    trace_max = trace.max()
    trace_min = trace.min()
    amplitude = trace_max - trace_min

    diff = np.gradient(trace)
    dt = np.gradient(time)
    time_derivative = np.divide(diff, dt)

    max_time_derivative = time_derivative.max()

    return (trace_max, CL, trace_min, *action_potential_duration(time, trace), time_derivative.max(), diastolic_depolarization_rate(time, trace), amplitude)


def run(input_file: Any):
    """Compute AP markers based on the input_file in the openCARP format.

    Args:
        input_file (Any): input file

    Note: prints the output to the command line for both the Vm and Cai
    """
    index_time = 0
    index_vm = 26    # Severi = 20, Loewe = 21, Fabbri = 22 (init file), Severi + BY = 36, Fabbri/Loewe + BY (init file) = 33
    index_cai = 4    # Severi/Fabbri/Loewe = 4, Severi + BY = 6, Fabbri/Loewe + BY (init file) = 5
    index_ki = 21
    index_nai = 22
    data = np.loadtxt(input_file, skiprows=1)

    time = data[:,index_time]      # ms
    vm = data[:,index_vm]          # mV
    cai = data[:,index_cai]*1e6    # nM
    ki = data[:,index_ki]
    nai = data[:,index_nai]

    indices_minimal_vm, _  = find_peaks(-data[:,index_vm], width=10)
    indices_minimal_cai, _ = find_peaks(-data[:,index_cai], width=10)
    
    indices_minimal_ki, _  = find_peaks(-data[:,index_ki], width=10)
    if len(indices_minimal_ki) == 0:
    	indices_minimal_ki = indices_minimal_vm
    	
    indices_minimal_nai, _ = find_peaks(-data[:,index_nai], width=10)
    if len(indices_minimal_nai) == 0:
    	indices_minimal_nai = indices_minimal_vm

    n = min(len(indices_minimal_cai), len(indices_minimal_vm), 
    		len(indices_minimal_ki), len(indices_minimal_nai))
    if(n > 2):
    	print(*metrics_keys, sep='\t')
    	for index in range(n-1):
    		i = indices_minimal_cai[index]
    		j = indices_minimal_cai[index+1]
    		markers_cai = markers(time[i:j], cai[i:j])

    		i = indices_minimal_vm[index]
    		j = indices_minimal_vm[index+1]
    		markers_vm = markers(time[i:j], vm[i:j])
    		
    		i = indices_minimal_ki[index]
    		j = indices_minimal_ki[index+1]
    		markers_ki = markers(time[i:j], ki[i:j])
    		
    		i = indices_minimal_nai[index]
    		j = indices_minimal_nai[index+1]
    		markers_nai = markers(time[i:j], nai[i:j])

    		print(*extract_metrics(markers_vm + markers_cai + markers_ki + markers_nai), sep='\t')
    else:
    	print('NO_STABLE_AP')


def compute_markers(input_file: Any) -> Any:
    """Compute AP markers based on the input_file in the openCARP format, while 
    considering only 5 last APs for the computation.

    Args:
        input_file (Any): input file

    Returns:
        Any: AP chracteristics 
             - Vm: Overshoot, CL, MDP, APD, Maxdvdt, DDR100
             - Cai: Overshoot, MDP, TD, TA
    """
    index_time = 0
    index_vm = 26   # Severi = 20, Loewe = 21, Fabbri = 22 (init file), Severi + BY = 36, Fabbri/Loewe + BY (init file) = 33
    index_cai = 4    # Severi/Fabbri/Loewe = 4, Severi + BY = 6, Fabbri/Loewe + BY (init file) = 5

    data = np.loadtxt(input_file,skiprows=1)
    indices_minimal, _ = find_peaks(-data[:,index_vm])

    num_APs = 6
    assert(len(indices_minimal) >= num_APs)

    # extract num_APs
    start_index = indices_minimal[-num_APs]
    end_index = indices_minimal[-1]
    data = data[start_index:end_index,:]

    time = data[:,index_time]      # ms
    vm = data[:,index_vm]          # mV
    cai = data[:,index_cai]*1e6    # nM

    # VM: OVershoot
    vm_overshoot = vm.max()

    # VM: CL
    indices_peaks, _ = find_peaks(vm)

    time_peaks = time[indices_peaks]
    periods = time_peaks[1:] - time_peaks[:-1]

    CL = statistics.mean(periods)

    # minimal diastolic potential
    vm_min= vm.min()

    # VM: Maxdvdt
    dv = np.gradient(vm)
    dt = np.gradient(time)
    dvdt = np.divide(dv, dt)
    Maxdvdt = dvdt.max()

    # computation of DDR and APD
    indices_diastolic, _ = find_peaks(-vm)

    start_index = 0
    end_index = indices_diastolic[0] - 1
    # action potential duration
    apd = []
    ddr100 = []
    for end_index in indices_diastolic:
        apd.append(action_potential_duration(time[start_index:end_index], vm[start_index:end_index]))
        # rate of 100 ms of diastolic depolarization
        ddr100.append(diastolic_depolarization_rate(time[start_index:end_index], vm[start_index:end_index]))
        start_index = end_index

    APD = np.array(apd)
    APD90 = statistics.mean(APD[:,0])
    APD50 = statistics.mean(APD[:,1])
    APD20 = statistics.mean(APD[:,2])
    DDR100 = statistics.mean(ddr100)

    ca_overshoot = cai.max()

    ca_min = cai.min()
    TA = cai.max() - cai.min()

    indices_cai_min, _ = find_peaks(-cai, width=10)
    assert(len(indices_cai_min) == (len(indices_diastolic) + 1))
    start_index = indices_cai_min[0]
    # end_index = indices_cai_min[1]
    td = []
    for end_index in indices_cai_min[1:]:
        td.append(action_potential_duration(time[start_index:end_index], cai[start_index:end_index]))
        start_index = end_index

    TD = np.array(td)
    TD90 = statistics.mean(TD[:,0])
    TD50 = statistics.mean(TD[:,1])
    TD20 = statistics.mean(TD[:,2])

    return vm_overshoot, CL, vm_min, APD90, APD50, APD20, Maxdvdt, DDR100, ca_overshoot, ca_min, TD90, TD50, TD20, TA


def print_metrics(vm_overshoot, CL, vm_min, APD90, APD50, APD20, Maxdvdt, DDR100, ca_overshoot, ca_min, TD90, TD50, TD20, TA):
    print(f'''{vm_min       :15g}     # vm_min       MDP (mV)
              {CL           :15g}     # CL           CL (ms)
              {Maxdvdt      :15g}     # Maxdvdt      (dV/dt)max (V/s)
              {APD20        :15g}     # APD20        APD20 (ms)
              {APD50        :15g}     # APD50        APD50 (ms)
              {APD90        :15g}     # APD90        APD90 (ms)
              {vm_overshoot :15g}     # vm_overshoot AP overshoot (mV)
              {DDR100       :15g}     # DDR100       DDR100 (mV/s)
              {ca_min       :15g}     # ca_min       [Ca]i,min (mM)
              {ca_overshoot :15g}     # ca_overshoot [Ca]i,max (mM)
              {TA           :15g}     # TA           CaT amplitutde (mM)
              {TD20         :15g}     # TD20         CaTD20 (ms)
              {TD50         :15g}     # TD50         CaTD50 (ms)
              {TD90         :15g}     # TD90         CaTD90 (ms)''')


def print_tableII(vm_overshoot, CL, vm_min, APD90, APD50, APD20, Maxdvdt, DDR100, ca_overshoot, ca_min, TD90, TD50, TD20, TA):
    print(f'''--------------------|--------------------
                   marker         |     Value
              --------------------|--------------------
              MDP (mV)            | {vm_min:10.2f}
              CL (ms)             | {CL:10.2f}
              (dV/dt)max (V/s)    | {Maxdvdt:10.2f}
              APD20 (ms)          | {APD20:10.2f}
              APD50 (ms)          | {APD50:10.2f}
              APD90 (ms)          | {APD90:10.2f}
              AP overshoot (mV)   | {vm_overshoot:10.2f}
              DDR100 (mV/s)       | {DDR100:10.2f}
              [Ca]i,min (mM)      | {ca_min:10.2f}
              [Ca]i,max (mM)      | {ca_overshoot:10.2f}
              CaT amplitutde (mM) | {TA:10.2f}
              CaTD20 (ms)         | {TD20:10.2f}
              CaTD50 (ms)         | {TD50:10.2f}
              CaTD90 (ms)         | {TD90:10.2f}
              --------------------|--------------------
              compare to Loewe et al. 2019 IEEE Tab. II''')
    

if __name__ == '__main__':

    if len(sys.argv) != 2:
        print(f"Usage: python {sys.argv[0]} INPUT_FILENAME")
        exit(1)

    infile = sys.argv[1]

    # print_metrics(*compute_markers(infile))
    run(infile)

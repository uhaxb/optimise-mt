# SPDX-FileCopyrightText: 2024 Karlsruhe Institute of Technology (KIT) <tomas.stary@kit.edu>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# this is the optimisation algorithm to tune the parameters of the Fabbri and Loewe model.

import scipy.optimize
import subprocess
import multiprocessing as mp
from math import log10, floor


from random import random

import sys
sys.path.append("..")
from run import parse_parameters

# https://stackoverflow.com/questions/4417546/constantly-print-subprocess-output-while-process-is-running
def execute(cmd:str):
	"""executes terminal command

	Args:
		cmd (str): contains terminal command

	Yields:
		str: contains one line of terminal command
	"""
	popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
	for stdout_line in iter(popen.stdout.readline, ""):
		yield stdout_line
	popen.stdout.close()
	return_code = popen.wait()
	#if return_code:
	#	raise subprocess.CalledProcessError(return_code, cmd)

def weighted_rms(simulation, target, sd, weight):
	return weight*((simulation-target)/sd)**2

def cost_lutz(simulation, target, sem, weight):
	diff = abs(simulation - target)
	if diff > sem:
		return weight*((simulation-target)/target)**2
	return 0
	
def cost_lutz_4(simulation, target, sd, weight):
	return weight*((simulation-target)/target)**4

def cost_fabbri(simulation, target, sem, weight):
	diff = abs(simulation - target)
	if diff > sem:
		return weight*((diff-sem)/sem)
	return 0

def cost_fabbri_sd(simulation, target, sd, weight):
	diff = abs(simulation - target)
	if diff > sd:
		return weight*((diff-sd)/sd)
	return 0
	
def cost_fluctuation_f(fluctuation):

	# [OS, CL, MDP, K_i, Na_i]
	weights = [0.0, 0.0, 0.0, 0.0, 0.0]
	thresholds = [0.03, 2.0, 0.03, 0.001, 0.05]
	vm_overshoot_amp = fluctuation[1]
	cl_amp = fluctuation[2]
	vm_min_amp = fluctuation[3]
	ki_overshoot_amp = fluctuation[4]
	nai_overshoot_amp = fluctuation[5]
	
	cost = 0.0
	
	for i in range(len(weights)):
		# skip index 0 for fluctuation because that's a flag used for another purpose
		if fluctuation[i+1] > thresholds[i]:
			cost += weights[i] * abs(fluctuation[i+1] - thresholds[i])
			
	print('Fluctuation vector:    ' + str(fluctuation))
	print('Fluctuation cost feedback:    ' + str(cost))
	return cost

def extract_simulation_metrics(header:list, markers:list):
	"""extract simulation metrics defined in compute_metrics

	Args:
		header (list): contains metrics file header
		markers (list): contains values of AP markers

	Returns:
		dict: contains metrics as key and related values
	"""

	simulation = dict()
	for key in header:
		index = header.index(key)
		simulation[key] = markers[index]

	return simulation
	
def run_simulation(filename:str, iso:bool, ach:bool, ifblock:bool) -> tuple:
	"""execute simulation

	Args:
		filename (str): filename
		iso (bool): simulation is Iso?
		ach (bool): simulation is ACh?
		ifblock (bool): simulation is Ifblock?

	Returns:
		tuple: contains 3 lists (L (line), header and fluctuations)
	"""
							
	assert (iso and ach) == False 
	command = ['make', filename]
	if iso == True:
		command = ['make', '-f', 'Makefile_iso', filename]
	elif ach == True:
		command = ['make', '-f', 'Makefile_ach', filename]
	elif ifblock == True:
		command = ['make', '-f', 'Makefile_ifblock', filename]
	for line in execute(command):
		print(line, end='')
	
	vm_overshoot_list = []
	cl_list = []
	vm_min_list = []
	ki_overshoot_list = []
	nai_overshoot_list = []
	
	simulation = dict()
	with open(filename, 'r') as f:
		first_line = f.readline()
		header = ['']
		# detect if read file contains valid data
		# otherwise return high cost value
		if 'NO_STABLE_AP' in first_line:
			return 1e10, header, [-1, 0, 0, 0, 0, 0]
		header = first_line.split()
		for line in f:
			# keep track of the cl development
			# to detect stability
			vm_overshoot_list.append(float(line.split()[0]))
			cl_list.append(float(line.split()[1]))
			vm_min_list.append(float(line.split()[2]))
			ki_overshoot_list.append(float(line.split()[14]))
			nai_overshoot_list.append(float(line.split()[15]))
			pass
			
	# need to deal with improbable edge case where DDR is not able to be calculated
	# but still writes metrics into the metrics file 
	
	L = line.split()
	L = ['1e10' if element is None else element for element in L]
	L = ['1e10' if (element == 'None') else element for element in L]
	
	# check if the cycle length (CL) has converged
	# otherwise return high cost function value
	
	fluctuation = [0,0,0,0,0,0]
	
	vm_overshoot_amp = 10.0
	cl_amp = 10.0
	vm_min_amp = 10.0
	ki_overshoot_amp = 10.0
	nai_overshoot_amp = 2.0

	if not (vm_overshoot_list == []): 
		vm_overshoot_amp = abs(max(vm_overshoot_list)-min(vm_overshoot_list))
	if not (cl_list == []): 
		cl_amp = abs(max(cl_list)-min(cl_list))
	if not (vm_min_list == []): 
		vm_min_amp = abs(max(vm_min_list)-min(vm_min_list))
	if not (ki_overshoot_list == []): 
		ki_overshoot_amp = abs(max(ki_overshoot_list)-min(ki_overshoot_list))
	if not (nai_overshoot_list == []): 
		nai_overshoot_amp = abs(max(nai_overshoot_list)-min(nai_overshoot_list))
	
	fluctuation[1] = vm_overshoot_amp
	fluctuation[2] = cl_amp
	fluctuation[3] = vm_min_amp
	fluctuation[4] = ki_overshoot_amp
	fluctuation[5] = nai_overshoot_amp
			
	return L, header, fluctuation

# utility function to shorten parameters because of file name length restrictions
def truncate_float(float_number:float, decimal_places:int) -> float:
	"""parameter digit shortening

	Args:
		float_number (float): parameter value as float
		decimal_places (int): decimal places as int

	Returns:
		float: shortened float represents parameter value
	"""
	magnitude = floor(log10(abs(float_number)))
	multiplier = 10 ** (decimal_places - magnitude - 1)

	return int(float_number * multiplier) / multiplier


def cost_function(x:list, *args:tuple) -> float:
	"""define cost function for optimization

	Args:
		x (list): contains parameters

	Returns:
		float: contains cost function value
	"""

	target	   = args[0]
	SD		   = args[1]
	weights	   = args[2]
	constrains = args[3]
	initial		= args[4]
	labels	   = args[5]
	optimize   = args[6]
	target_iso = args[7]
	weights_iso = args[8]
	SEM		 = args[9]
	target_ach = args[10]
	weights_ach =  args[11]
	target_ifblock = args[12]
	weights_ifblock =  args[13]
	CL_ratio_target = target['CL'] / target_iso['CL']
	CL_ratio_target_ach = target['CL'] / target_ach['CL']
	
	parameters = ""
	parameters_ifblock = ""
	i = 0
	
	# storing gf_k for keeping gf_na/gf_k = 0.5927
	gf_k_temp = 0
	for label in labels:
		if label in optimize:
			value = truncate_float(x[i], 6)
			# storing gf_k for keeping gf_na/gf_k = 0.5927
			if label == 'Gf_K':
				gf_k_temp = value
			i += 1
		else:
			value = truncate_float(initial[label], 6)
			# keeping gf_na at the ratio gf_na/gf_k = 0.5927
			if (label == 'Gf_Na') and ('Gf_K' in optimize) and not (gf_k_temp == 0):
				value = truncate_float(gf_k_temp*0.5927, 6)
		parameters += f"{value}_"
		
		# building a parameter string setting funny current to zero
		if label == 'Gf_Na' or label == 'Gf_K':
			value = 0.0
		parameters_ifblock += f"{value}_"

	P = parse_parameters(parameters)
	for key in P.keys():
		if P[key] < constrains[key][0] or P[key] > constrains[key][1]:
			print(str(key) + '       ' + str(P[key]) + '  out of bounds!')
			return 1e10 + random() + 1
	
	# one specific filename gets created for every variation of simulation
	filename = "dat/metricsLoewe_" + parameters + "dur100000ms.dat"
	filename_iso = "dat/iso_metricsLoewe_" + parameters + "dur100000ms.dat"
	filename_ach = "dat/ach_metricsLoewe_" + parameters + "dur100000ms.dat"
	filename_ifblock = "dat/ifblock_metricsLoewe_" + parameters_ifblock + "dur100000ms.dat"
	
	# adjust here if additional variations get added or left out
	inputs = [(filename, False, False, False), (filename_iso, True, False, False), (filename_ach, False, True, False), (filename_ifblock, False, False, True)] 
	# adjust to number of cores/threads/simulations ran in parallel
	number_of_processes = 4
	results = mp.Pool(number_of_processes).starmap(run_simulation, inputs)
	
	# one data structure each per variation
	L_ach = 1
	L = 1
	L_iso = 1
	L_ifblock = 1
	
	L = results[0][0] 
	header = results[0][1] 
	L_iso = results[1][0] 
	header_iso = results[1][1] 
	L_ach = results[2][0] 
	header_ach = results[2][1]
	L_ifblock = results[3][0] 
	header_ifblock = results[3][1]

	fluctuation = results[0][2]
	fluctuation_iso = results[1][2]
	fluctuation_ach = results[2][2]
	fluctuation_ifblock = results[3][2]
	
	
	
	# return heavy cost punishments if simulation shows no APs
	if (L_iso == 1e10) or (L == 1e10) or (L_ach == 1e10) or (L_ifblock == 1e10):
		return 1e10 + random() + 1
	
	# return heavy cost punishments if overshoot is under 10mv
	if (float(L[0]) < 10.0) or (float(L_iso[0]) < 10.0) or (float(L_ach[0]) < 10.0) or (float(L_ifblock[0]) < 10.0):
		return 1e10 + random() + 1

	simulation = extract_simulation_metrics(header, [float(i) for i in L])
	simulation_iso = extract_simulation_metrics(header_iso, [float(i) for i in L_iso])
	simulation_ach = extract_simulation_metrics(header_ach, [float(i) for i in L_ach])
	simulation_ifblock = extract_simulation_metrics(header_ifblock, [float(i) for i in L_ifblock])

	cost = 0
	cost_noiso = 0
	cost_iso = 0
	cost_ach = 0
	cost_ifblock = 0

	detailed_cost = []
	detailed_cost_iso = []
	detailed_cost_noiso = []
	detailed_cost_ach = []
	detailed_cost_ifblock = []
	additional_costs = []
	
	for key in simulation.keys():
		cost += cost_lutz(simulation[key], target[key], SEM[key], weights[key])
		cost_noiso += cost_lutz(simulation[key], target[key], SEM[key], weights[key])
		detailed_cost_noiso.append(round(cost_lutz(simulation[key], target[key], SEM[key], weights[key]),4))
	
	for key in simulation_iso.keys():
		cost += cost_lutz(simulation_iso[key], target_iso[key], SEM[key], weights_iso[key])
		cost_iso += cost_lutz(simulation_iso[key], target_iso[key], SEM[key], weights_iso[key])
		detailed_cost_iso.append(round(cost_lutz(simulation_iso[key], target_iso[key], SEM[key], weights_iso[key]),4))
		
	for key in simulation_ach.keys():
		cost += cost_lutz(simulation_ach[key], target_ach[key], SEM[key], weights_ach[key])
		cost_ach += cost_lutz(simulation_ach[key], target_ach[key], SEM[key], weights_ach[key])
		detailed_cost_ach.append(round(cost_lutz(simulation_ach[key], target_ach[key], SEM[key], weights_ach[key]),4))
		
	for key in simulation_ifblock.keys():
		cost += cost_lutz(simulation_ifblock[key], target_ifblock[key], SEM[key], weights_ifblock[key])
		cost_ifblock += cost_lutz(simulation_ifblock[key], target_ifblock[key], SEM[key], weights_ifblock[key])
		detailed_cost_ifblock.append(round(cost_lutz(simulation_ifblock[key], target_ifblock[key], SEM[key], weights_ifblock[key]),4))
	
	# Include the percentage of CL decrease from regular to iso into the cost function
	# iso_weight = 0.0 disables it
	iso_weight = 0.0
	CL_ratio = simulation['CL'] / simulation_iso['CL']
	if CL_ratio < 1.3 or CL_ratio > 1.5:
		iso_weight *= 2
	CL_ratio_cost = cost_lutz(simulation=CL_ratio, target=CL_ratio_target, sem=0.02, weight=iso_weight) # sem=0.02 default
	additional_costs.append(round(CL_ratio_cost, 4))
	cost += CL_ratio_cost
	cost_iso += CL_ratio_cost
	
	# Include the percentage of CL increase from regular to ach into the cost function
	# ach_weight = 0.0 disables it
	ach_weight = 0.0
	CL_ratio_ach = simulation['CL'] / simulation_ach['CL']
	if CL_ratio_ach > 0.9 or CL_ratio_ach < 0.7:
		ach_weight *= 2
	CL_ratio_cost_ach = cost_lutz(simulation=CL_ratio_ach, target=CL_ratio_target_ach, sem=0.02, weight=ach_weight) # sem=0.02 default
	additional_costs.append(round(CL_ratio_cost_ach, 4))
	cost += CL_ratio_cost_ach
	cost_ach += CL_ratio_cost_ach
	
	cost_fluctuation = 0
	if not (fluctuation[0] == -1):
		cost_fluctuation += cost_fluctuation_f(fluctuation)
		
	if not (fluctuation_iso[0] == -1):
		cost_fluctuation += cost_fluctuation_f(fluctuation_iso)
		cost_iso += cost_fluctuation_f(fluctuation_iso)
		
	if not (fluctuation_ach[0] == -1):
		cost_fluctuation += cost_fluctuation_f(fluctuation_ach)	
		cost_ach += cost_fluctuation_f(fluctuation_ach)
		
	if not (fluctuation_ifblock[0] == -1):
		cost_fluctuation += cost_fluctuation_f(fluctuation_ifblock)	
		cost_ifblock += cost_fluctuation_f(fluctuation_ifblock)
		
	additional_costs.append(round(cost_fluctuation_f(fluctuation), 4))
	additional_costs.append(round(cost_fluctuation_f(fluctuation_iso), 4))
	additional_costs.append(round(cost_fluctuation_f(fluctuation_ach), 4))
	additional_costs.append(round(cost_fluctuation_f(fluctuation_ifblock), 4))
	
	cost += cost_fluctuation
	
	for i in range(len(detailed_cost_noiso)):
		detailed_cost.append(round(detailed_cost_noiso[i] + detailed_cost_iso[i] + detailed_cost_ach[i] + detailed_cost_ifblock[i], 2))
		
		
	
	print(f"Current cost: {cost}")
	print(f"Current cost (no iso): {cost_noiso}")
	print(f"Current cost (iso): {cost_iso}")
	print(f"Current cost (ach): {cost_ach}")
	print(f"Current cost (ifblock): {cost_ifblock}")
	print(f"Current cost-vector: {detailed_cost}")
	print(f"Current cost-vector (no iso): {detailed_cost_noiso}")
	print(f"Current cost-vector (iso): {detailed_cost_iso}")
	print(f"Current cost-vector (ach): {detailed_cost_ach}")
	print(f"Current cost-vector (ifblock): {detailed_cost_ifblock}")
	print(f"Current CL-ratio-cost (iso, ach), DDR-ratio-cost(iso, ach), CL-fluctuation(reg, iso, ach): ")
	print(f"{additional_costs} ")

	
	# writing total cost development into a file
	filename = "dat/total_costs_dur30000ms.dat"
	with open(filename, 'a+') as f:
		f.write(str(cost) + '\t' + str(cost_noiso) + '\t' + str(cost_iso) + '\t'
		 + str(cost_ach) + '\t' + str(cost_ifblock) + '\t' + str(cost_fluctuation) + '\n')
		   
	return cost

def get_opt_args() -> tuple:
	"""calculate optimised parameters

	Returns:
		tuple: contains (target, SD, weights, constrains, initial, labels, optimize, target_iso,
		  weights_iso, SEM, target_ach, weights_ach, target_ifblock, weights_ifblock)
	"""

	# Fabbri et al. 2017 optimised parameter (Table A1)
	# g Kur		   μS			   1.539e-4					   0.9895–1.633 × 10 −4
	# P CaT		   nA m M −1	   0.04132					  0.04132–0.05191
	# P CaL		   nA m M −1	   0.4578					  0.3475–0.5262
	# V dL		   mV			   -16.45					  −20.97 to −13.73
	# k dL		   mV			   4.337					  3.892–4.401
	# I NaK,max	   nA			   0.08105					  0.0623–0.1063
	# K NaCa	   nA			   3.343					  2.733–4.964
	# K up		   n M			   286.1					  204.6–356.2
	# k s		   s −1			   1.480e8					 1.460–1.847 × 10 8
	# τ difCa	   s			   5.469e-5					 5.469–7.951 × 10 −5
	# kf CM		   m M s −1		   1.642e6					 1.164–1.839 × 10 6
	# kf CQ		   m M s −1		   175.4					  155.2–194.6

	# optimised parameters from Fabbri et al., note that they are
	# different set than in Loewe et al. 2019 --> commented out
	# value are according to Loewe model
	initial = dict()
	initial['GKur']		 = 1.539e-4
	initial['P_CaT']	 = 0.04132
	initial['P_CaL']	 = 0.4578
	initial['V_dL']		 = -16.45
	initial['k_dL']		 = 4.337
	initial['V_fL']		   = -37.4
	initial['k_fL']			= 5.3 
	initial['INaK_max']	 = 0.08105
	initial['K_NaCa']	 = 3.343
	
	initial['Gto']			  = 0.0035 
	initial['GKr_']			  = 0.00424
	initial['GKs_']			  = 0.00065 
	initial['Gf_K']			  = 0.00268 
	initial['Gf_Na']		  = 0.00159 

	initial['EC50_SK']		  = 0.7 
	initial['n_SK']			  = 2.2 
	initial['GSK']			  = 0.000165 
	initial['dynamic_Ki_Nai'] = 1 
	
	initial['GKACh']			  = 0.00345
			


	# initial ranges -- TODO: change to more meaningfull values
	constrains = dict()
		
	constrains['INaK_max']		 = (0.08105, 0.08105*1.75)
	constrains['P_CaL']	 = (0.4578*0.5, 0.4578*2)
	constrains['k_dL']	 = (3.0,7.0)
	constrains['V_dL']		 = (-25.0, -10.0)
	constrains['k_fL']		 = (3.0, 7.0)
	constrains['V_fL']		   = (-50.0, -25.0)
	constrains['GKur']			= (1.539e-4*0.5, 1.539e-4*2.0)
	constrains['Gto']	 = (0.0035*0.5, 0.0035*2)
	constrains['GKr_']	 = (0.00424*0.25, 0.00424*2.0) #Danielson experimental
	constrains['GKs_']		 = (0.00013*0.5, 0.00065*2.0) #(VW, Fabbri)
	constrains['Gf_K']		 = (0.00268*0.75, 0.00268*1.5)
	constrains['Gf_Na'] = (0,1)
	constrains['GSK']	 = (0.000165*0.5, 0.000165*1.3)# New ISK from Loewe paper 2019
	constrains['P_CaT']	 = (0.04132*0.25, 0.04132*4.0)#
	constrains['K_NaCa']			  = (3.343*0.5, 3.343*2)
	constrains['GKACh']			  = (0.00345/1000, 0.00345)
	
	constrains['EC50_SK']			  = (0, 1)
	constrains['n_SK']			  = (0,5)
	constrains['dynamic_Ki_Nai']			  = (0,1)


	# Fabbri et al. 2017 page 2392:
	# We set weight i = 2 for MDP, CL and CL prolongation
	# induced by Cs + , whereas weight i was set to 1 for all other
	# features.
	weights = dict()	
	weights['vm_min']		= 0.0
	weights['CL']			= 9.0
	weights['Maxdvdt']		= 0.0
	weights['APD20']		= 0
	weights['APD50']		= 0
	weights['APD90']		= 0
	weights['vm_overshoot'] = 0.0
	weights['DDR100']		= 0.0
	weights['ca_min']		= 0
	weights['ca_overshoot'] = 0
	weights['TA']			= 0
	weights['TD20']			= 0
	weights['TD50']			= 0
	weights['TD90']			= 0	
	weights['ki_overshoot']			= 0
	weights['nai_overshoot']			= 0

	weights_iso = dict()
	weights_iso['vm_min']		= 0 
	weights_iso['CL']			= 3
	weights_iso['Maxdvdt']		= 0
	weights_iso['APD20']		= 0
	weights_iso['APD50']		= 0
	weights_iso['APD90']		= 0
	weights_iso['vm_overshoot'] = 0
	weights_iso['DDR100']		= 0
	weights_iso['ca_min']		= 0
	weights_iso['ca_overshoot'] = 0
	weights_iso['TA']			= 0
	weights_iso['TD20']			= 0
	weights_iso['TD50']			= 0
	weights_iso['TD90']			= 0	
	weights_iso['ki_overshoot']			= 0
	weights_iso['nai_overshoot']			= 0
	
	weights_ach = dict()	
	weights_ach['vm_min']		= 0 
	weights_ach['CL']			= 3
	weights_ach['Maxdvdt']		= 0
	weights_ach['APD20']		= 0
	weights_ach['APD50']		= 0
	weights_ach['APD90']		= 0
	weights_ach['vm_overshoot'] = 0 
	weights_ach['DDR100']		= 0
	weights_ach['ca_min']		= 0
	weights_ach['ca_overshoot'] = 0
	weights_ach['TA']			= 0
	weights_ach['TD20']			= 0
	weights_ach['TD50']			= 0
	weights_ach['TD90']			= 0	
	weights_ach['ki_overshoot']			= 0
	weights_ach['nai_overshoot']			= 0
	
	weights_ifblock = dict()	
	weights_ifblock['vm_min']		= 0 
	weights_ifblock['CL']			= 3
	weights_ifblock['Maxdvdt']		= 0
	weights_ifblock['APD20']		= 0
	weights_ifblock['APD50']		= 0
	weights_ifblock['APD90']		= 0
	weights_ifblock['vm_overshoot'] = 0 
	weights_ifblock['DDR100']		= 0
	weights_ifblock['ca_min']		= 0
	weights_ifblock['ca_overshoot'] = 0
	weights_ifblock['TA']			= 0
	weights_ifblock['TD20']			= 0
	weights_ifblock['TD50']			= 0
	weights_ifblock['TD90']			= 0	
	weights_ifblock['ki_overshoot']			= 0
	weights_ifblock['nai_overshoot']			= 0
	
	# Fabbri et al. 2017 Table 5
	# MDP			   mV		   −61.7	  6.1
	# CL			   ms		   828		  21
	# (dV/dt) max	   V s −1	   4.6		  1.7
	# APD 20		   ms		   64.9		  23.9
	# APD 50		   ms		   101.5	  38.2
	# APD 90		   ms		   143.5	  49.3
	# OS			   mV		   16.4		  0.9
	# DDR 100		   mV s −1	   48.9		  25.5

	# Fabbri et al. 2017 Table 6 from Verkerk et al. 2013
	# Ca i range	  n M		 105–220
	# TA			  n M		 115
	# TD 20			  ms		 138.9
	# TD 50			  ms		 217.4
	# TD 90			  ms		 394.0

	target = dict()
	target['vm_min']	   = -61.7
	target['CL']		   = 828
	target['Maxdvdt']	   = 4.6
	target['APD20']		   = 64.9
	target['APD50']		   = 101.5
	target['APD90']		   = 143.5
	target['vm_overshoot'] = 16.4
	target['DDR100']	   = 48.9

	target['ca_min']	   = 105
	target['ca_overshoot'] = 220
	target['TA']		   = 115
	target['TD20']		   = 138.9
	target['TD50']		   = 217.4
	target['TD90']		   = 394.0
	
	target['nai_overshoot']			= 1e-5
	target['ki_overshoot']			= 1e-5
	
	# Targets for simulated Fabbri model using 1mm of iso

	
	target_iso = dict()
	target_iso['vm_min']	   = -57.46 
	target_iso['CL']		   = 590 
	target_iso['Maxdvdt']	   = 11.64 
	target_iso['APD20']		   = 75 
	target_iso['APD50']		   = 119 
	target_iso['APD90']		   = 152 
	target_iso['vm_overshoot'] = 16.0 
	target_iso['DDR100']	   = 52.12

	target_iso['ca_min']	   = 98.23 
	target_iso['ca_overshoot'] = 222.0 
	target_iso['TA']		   = 123.77 
	target_iso['TD20']		   = 134.0 
	target_iso['TD50']		   = 188.0 
	target_iso['TD90']		   = 459.0 
	
	target_iso['ki_overshoot']			= 1e-5
	target_iso['nai_overshoot']			= 1e-5
	
	target_ach = dict()
	target_ach['vm_min']	   = 1e-5
	target_ach['CL'] = 1034 # 58 bpm / -20% from 72.5bpm
	target_ach['Maxdvdt']	   = 1e-5
	target_ach['APD20']		   = 1e-5
	target_ach['APD50']		   = 1e-5
	target_ach['APD90']		   = 1e-5
	target_ach['vm_overshoot'] = 1e-5
	target_ach['DDR100']	   = 1e-5

	target_ach['ca_min']	   = 1e-5
	target_ach['ca_overshoot'] = 1e-5
	target_ach['TA']		   = 1e-5
	target_ach['TD20']		   = 1e-5
	target_ach['TD50']		   = 1e-5
	target_ach['TD90']		   = 1e-5
	
	target_ach['ki_overshoot']			= 1e-5
	target_ach['nai_overshoot']			= 1e-5
	
	target_ifblock = dict()
	target_ifblock['vm_min']	   = 1e-5
	target_ifblock['CL'] = 1043 # 828*1.26 (Verkerk 2007b) If_block 57.5bpm 
	target_ifblock['Maxdvdt']	   = 1e-5
	target_ifblock['APD20']		   = 1e-5
	target_ifblock['APD50']		   = 1e-5
	target_ifblock['APD90']		   = 1e-5
	target_ifblock['vm_overshoot'] = 1e-5
	target_ifblock['DDR100']	   = 1e-5

	target_ifblock['ca_min']	   = 1e-5
	target_ifblock['ca_overshoot'] = 1e-5
	target_ifblock['TA']		   = 1e-5
	target_ifblock['TD20']		   = 1e-5
	target_ifblock['TD50']		   = 1e-5
	target_ifblock['TD90']		   = 1e-5
	
	target_ifblock['ki_overshoot']			= 1e-5
	target_ifblock['nai_overshoot']			= 1e-5
	
	# Fabbri et al. 2017 claims to use SEM for the computations, here
	# is the difference explained:
	# https://doi.org/10.1177/0253717620933419
	# TODO: should we compute SEM or rather update the cost function?
	# here n = 3
	n_experiments = 3
	
	SD = dict()
	SD['vm_min']	   = 6.1
	SD['CL']		   = 21
	SD['Maxdvdt']	   = 1.7 #0.5 #1.7
	SD['APD20']		   = 23.9/2 #6.5 #23.9
	SD['APD50']		   = 38.2/2 #10 #38.2
	SD['APD90']		   = 49.3/2 #15 #49.3
	SD['vm_overshoot'] = 0.9
	SD['DDR100']	   = 25.5 #5 #25.5
	
	# Fabbri et al. 2017 page 2392:
	# Because the experimental data on the calcium transient
	# are from only one cell, we arbitrarily set the SEM of each of
	# the calcium transient features to 40% in our optimization
	# procedure.

	# NOTE: not sure if this multiplication does really make sense as
	# it allows for quite a large range that do not have any effects
	# on the cost
	
	# TODO include sd_factor in weights
	sd_factor = 0.2
	
	SD['ca_min']	   = sd_factor*105#0.4*105
	SD['ca_overshoot'] = sd_factor*220#0.4*220
	SD['TA']		   = sd_factor*115#0.4*115
	SD['TD20']		   = sd_factor*138.9#0.4*138.9
	SD['TD50']		   = sd_factor*217.4#0.4*217.4
	SD['TD90']		   = sd_factor*394.0#0.4*394.0
	
	SD['ki_overshoot']			= 1
	SD['nai_overshoot']			= 1
	
	# Calculating SEM from SD
	# SEM = SD / sqrt(n)
	
	SEM = dict()
	
	# for n=1 SEM = SD
	# calcium transient features based on only one cell
	SEM = dict(SD)
	sqrt_n = n_experiments **(0.5)
	SEM['vm_min']	   		= SD['vm_min'] / (sqrt_n)
	SEM['CL']		   		= 2 #SD['CL'] / (sqrt_n)
	SEM['Maxdvdt']	   		= SD['Maxdvdt'] / (sqrt_n)
	SEM['APD20']		   	= SD['APD20'] / (sqrt_n)
	SEM['APD50']		  	= SD['APD50'] / (sqrt_n)
	SEM['APD90']		  	= SD['APD90'] / (sqrt_n)
	SEM['vm_overshoot'] 	= SD['vm_overshoot'] / (sqrt_n)
	SEM['DDR100']	   		= SD['DDR100'] / (sqrt_n)
	
	SEM['ki_overshoot']			= 1
	SEM['nai_overshoot']			= 1
	
	# labels as in parse_parameters(parameters)
	labels = ['INaK_max', 'P_CaL', 'k_dL', 'V_dL', 'k_fL', 'V_fL', 'GKur',
			  'Gto', 'GKr_', 'GKs_', 'Gf_K', 'Gf_Na', 'EC50_SK', 'n_SK', 
			  'GSK', 'dynamic_Ki_Nai', 'P_CaT', 'K_NaCa', 'GKACh'] 

	# Note: lists needs to be filled in the order the labels are ordered
	# optimize = ['INaK_max', 'P_CaL', 'k_dL', 'V_dL', 'k_fL', 'V_fL', 'GKur', 'GKr_', 'P_CaT', 'K_NaCa']
	optimize = ['INaK_max', 'P_CaL', 'k_dL', 'V_dL', 'k_fL', 'V_fL', 'GKur',
			  'Gto', 'GKr_', 'GKs_', 'Gf_K',# 'Gf_Na', 
			  'GSK', 'P_CaT', 'K_NaCa', 'GKACh']
	
				

	return target, SD, weights, constrains, initial, labels, optimize, target_iso, weights_iso, SEM, target_ach, weights_ach, target_ifblock, weights_ifblock

def run():
	cost_args		   = get_opt_args()
	constrains		   = cost_args[3]
	initial			   = cost_args[4]	
	labels			   = cost_args[5]
	optimize_variables = cost_args[6]
	xopt_array = []
	
	number_of_optimization_runs = 1
	ftol = 0.0001 # tolerance for convergence
	max_iterations = 500
	max_func_eval = 1000

	bounds = []
	for key in optimize_variables:
		bounds.append((constrains[key][0], constrains[key][1]))
		
	x = []
	for label in labels:
		if label in optimize_variables:
			x.append(initial[label])
	

	for i in range(number_of_optimization_runs):
		# this will take initial simplex as a random points on elipsoid
		# with the center at initial and height of the difference between
		# the constrains edges
		initial_simplex = [[] for _ in range(len(optimize_variables) + 1)]
		for vortex in initial_simplex:
			point = dict()
			norm  = dict()
			for label in optimize_variables:
				norm[label] = random()
				point[label] = norm[label]*( constrains[label][1] - constrains[label][0]) + constrains[label][0]
				vortex.append(point[label])
		options = {'initial_simplex': initial_simplex,'adaptive': False, 'maxiter': max_iterations, 'maxfev': max_func_eval}
		xopt = scipy.optimize.minimize(fun=cost_function, x0=x, args=cost_args, method='Nelder-Mead', bounds=bounds, options=options, tol=ftol)
		# writing status of current best solution into a file
		filename = "dat/END_OF_RUN__" + str(i+1) + ".dat"
		with open(filename, 'a+') as f:
			f.write(str(xopt))
			f.write(str(xopt.x))			
		xopt_array.append(xopt)
	# writing full array of results into a file
		filename = "dat/END_RESULTS.dat"
		with open(filename, 'w+') as f:
			f.write(str(xopt_array))
	

if __name__ == '__main__':
	run()

# SPDX-FileCopyrightText: 2024 Karlsruhe Institute of Technology (KIT) <tomas.stary@kit.edu>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import fileinput
from compute_metrics import print_metrics
        
def run():
    for line in fileinput.input():
        pass
    print_metrics(*[float(i) for i in line.split()])
    
if __name__ == '__main__':
    run()

-47.787168          # Vm
-                   # Lambda
-                   # delLambda
-                   # Tension
-                   # Ke
-                   # Nae
-                   # Cae
-0.0265935          # Iion
-                   # tension_component
-                   # illum
Loewe_iso
0.409551           # Init_Ca_jsr    # Ca_jsr
0.435148           # Init_Ca_nsr    # Ca_nsr
6.226104e-5        # Init_Ca_sub    # Ca_sub
9.15641e-6         # Init_Cai       # Cai
4.595622e-10       # Init_I_Ca_rel  # I
140                # Init_Ki        # Ki
5                  # Init_Nai       # Nai
6.181512e-9        # Init_O_Ca_rel  # O
0.23               # Init_PLBp      # PLBp
0.069199           # Init_RI_Ca_rel # RI
0.9308             # Init_R_Ca_rel  # R_1
0.00277            # Init_a         # a
0.0328833333333333 # Init_cAMP      # cAMP
0.001921           # Init_dL        # dL
0.268909           # Init_dT        # dT
0.217311           # Init_fCMi      # fCMi
0.158521           # Init_fCMs      # fCMs
0.138975           # Init_fCQ       # fCQ
0.844449           # Init_fCa       # fCa
0.846702           # Init_fL        # fL
0.020484           # Init_fT        # fT
0.017929           # Init_fTC       # fTC
0.259947           # Init_fTMC      # fTMC
0.653777           # Init_fTMM      # fTMM
0.003058           # Init_h         # h
0.003058           # Init_h_WT      # h_WT
0.447724           # Init_m         # m
0.447724           # Init_m_WT      # m_WT
0.1162             # Init_n         # n
0.011068           # Init_paF       # paF
0.283185           # Init_paS       # paS
0.709051           # Init_piy       # piy
0.430836           # Init_q         # q
0.014523           # Init_r_to      # r
0.011845           # Init_r_Kur     # r_Kur
0.845304           # Init_s         # s_Kur
0.0685782388661923 # Init_x         # x
0.009508           # Init_y         # y

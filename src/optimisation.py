# SPDX-FileCopyrightText: 2024 Karlsruhe Institute of Technology (KIT) <tomas.stary@kit.edu>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# this is the optimisation algorithm to tune the parameters of the Fabbri and Loewe model.

import scipy.optimize
# from optimparallel import minimize_parallel
import subprocess

from random import random

import sys
sys.path.append("..")
from run import parse_parameters

# https://stackoverflow.com/questions/4417546/constantly-print-subprocess-output-while-process-is-running
def execute(cmd):
	popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
	for stdout_line in iter(popen.stdout.readline, ""):
		yield stdout_line
	popen.stdout.close()
	return_code = popen.wait()
	if return_code:
		raise subprocess.CalledProcessError(return_code, cmd)

def weighted_rms(simulation, target, sd, weight):
	return weight*((simulation-target)/sd)**2

def cost_lutz(simulation, target, sd, weight):
	return weight*((simulation-target)/target)**2

def cost_fabbri(simulation, target, sem, weight):
	diff = abs(simulation - target)
	if diff > sem:
		return weight*((diff-sem)/sem)
	return 0

def cost_fabbri_sd(simulation, target, sd, weight):
	diff = abs(simulation - target)
	if diff > sd:
		return weight*((diff-sd)/sd)
	return 0

def extract_simulation_metrics(header, markers):
	simulation = dict()
	for key in header:
		index = header.index(key)
		simulation[key] = markers[index]

	return simulation
	
def run_simulation(filename, iso, cao):
	assert (iso and cao) == False 
	command = ['make', filename]
	if iso == True:
		command = ['make', '-f', 'Makefile_iso', filename]
	elif cao == True:
		command = ['make', '-f', 'Makefile_cao', filename]
	for line in execute(command):
		print(line, end='')

	simulation = dict()
	with open(filename, 'r') as f:
		first_line = f.readline()
		print(first_line)
		header = ['']
		# detect if read file contains valid data
		# otherwise return high cost value
		if 'N' in first_line:
			return 1e10, header
		header = first_line.split()
		for line in f:
			pass
			
	# need to deal with improbable edge case where DDR is not able to be calculated
	# but still writes metrics into the metrics file 
	
	L = line.split()
	L = ['1e10' if element is None else element for element in L]
	L = ['1e10' if (element == 'None') else element for element in L]
	
	return L, header

# utility function to shorten parameters because of file name length restrictions
# TODO: put into seperate itility.py
def truncate_float(float_number, decimal_places):
    multiplier = 10 ** decimal_places
    return int(float_number * multiplier) / multiplier


def cost_function(x, *args):

	target	   = args[0]
	SD		   = args[1]
	weights	   = args[2]
	constrains = args[3]
	initial		= args[4]
	labels	   = args[5]
	optimize   = args[6]
	target_iso = args[7]
	weights_iso = args[8]
	SEM		 = args[9]
	
	# calculate sum of used weights for normalization of the costs function(s)
	weights_sum = sum(weights.values())
	weights_length = len(weights)
	weights_sum_iso = sum(weights_iso.values())
	weights_length_iso = len(weights_iso)
	
	assert weights_sum > 0
	assert weights_sum >= weights_length
	assert weights_sum_iso > 0
	#assert weights_sum_iso >= weights_length_iso
	
	normalization_factor = weights_length / weights_sum
	normalization_factor_iso = weights_length_iso / weights_sum_iso
	

	parameters = ""
	i = 0
	for label in labels:
		if label in optimize:
			value = truncate_float(x[i], 8)
			i += 1
		else:
			value = truncate_float(initial[label], 8)
		parameters += f"{value}_"

	P = parse_parameters(parameters)
	for key in P.keys():
		if P[key] < constrains[key][0] or P[key] > constrains[key][1]:
			return 1e10
	
	# check if parameter set yields APs in low calcium mode
	# otherwise return high punishment value
			
	filename = "dat/cao_metricsLoewe_" + parameters + "dur120000ms.dat"
	L, header = run_simulation(filename, iso=False, cao=True)
	if (L == 1e10):
		return L
	else:
		if float(L[0]) < 10.0:
			print ("CAO OVERSHOOT <<<< 10.0")
			return 1e10

	filename = "dat/metricsLoewe_" + parameters + "dur120000ms.dat"
	L, header = run_simulation(filename, iso=False, cao=False)
	if (L == 1e10):
		return L
	
	simulation = extract_simulation_metrics(header, [float(i) for i in L])

	cost = 0
	cost_noiso = 0
	cost_iso = 0
	normalized_cost = 0
	normalized_cost_noiso = 0
	normalized_cost_iso = 0
	detailed_cost = []
	detailed_cost_iso = []
	detailed_cost_noiso = []
	
	for key in simulation.keys():
		#cost += weighted_rms(simulation[key], target[key], SD[key], weights[key])
		#cost_noiso += weighted_rms(simulation[key], target[key], SD[key], weights[key])
		#detailed_cost_noiso.append(round(weighted_rms(simulation[key], target[key], SD[key], weights[key]),2))
		#cost += cost_lutz(simulation[key], target[key], SD[key], weights[key])
		#cost_noiso += cost_lutz(simulation[key], target[key], SD[key], weights[key])
		#detailed_cost_noiso.append(round(cost_lutz(simulation[key], target[key], SD[key], weights[key]),2))
		cost += cost_fabbri(simulation[key], target[key], SEM[key], weights[key])
		cost_noiso += cost_fabbri(simulation[key], target[key], SEM[key], weights[key])
		detailed_cost_noiso.append(round(cost_fabbri(simulation[key], target[key], SEM[key], weights[key]),2))
	
	
	# run second experiment with isoprenaline activated using a second Makefile	   
	
	filename = "dat/iso_metricsLoewe_" + parameters + "dur120000ms.dat"
	L, header = run_simulation(filename, iso=True, cao=False)
	if (L == 1e10):
		return L
	
	simulation = extract_simulation_metrics(header, [float(i) for i in L])

	for key in simulation.keys():
		#cost += weighted_rms(simulation[key], target_iso[key], SD[key], weights_iso[key])
		#cost_iso += weighted_rms(simulation[key], target_iso[key], SD[key], weights_iso[key])
		#detailed_cost_iso.append(round(weighted_rms(simulation[key], target_iso[key], SD[key], weights_iso[key]),2))
		#cost += cost_lutz(simulation[key], target_iso[key], SD[key], weights_iso[key])
		#cost_iso += cost_lutz(simulation[key], target_iso[key], SD[key], weights_iso[key])
		#detailed_cost_iso.append(round(cost_lutz(simulation[key], target_iso[key], SD[key], weights_iso[key]),2))
		cost += cost_fabbri(simulation[key], target_iso[key], SEM[key], weights_iso[key])
		cost_iso += cost_fabbri(simulation[key], target_iso[key], SEM[key], weights_iso[key])
		detailed_cost_iso.append(round(cost_fabbri(simulation[key], target_iso[key], SEM[key], weights_iso[key]),2))
		
	for i in range(len(detailed_cost_noiso)):
		detailed_cost.append(round(detailed_cost_noiso[i] + detailed_cost_iso[i], 2))
		
	normalized_cost = cost * (normalization_factor + normalization_factor_iso) / 2
	normalized_cost_noiso = cost_noiso * normalization_factor
	normalized_cost_iso = cost_iso * normalization_factor_iso
		
	
	print(f"Current cost: {cost}")
	print(f"Current cost (no iso): {cost_noiso}")
	print(f"Current cost (iso): {cost_iso}")
	print(f"Current cost (normalized): {normalized_cost}")
	print(f"Current cost (no iso)(normalized): {normalized_cost_noiso}")
	print(f"Current cost (iso)(normalized): {normalized_cost_iso}")
	print(f"Current cost-vector: {detailed_cost}")
	print(f"Current cost-vector (no iso): {detailed_cost_noiso}")
	print(f"Current cost-vector (iso): {detailed_cost_iso}")
	
	# writing total cost development into a file
	filename = "dat/total_costs_dur120000ms.dat"
	with open(filename, 'a+') as f:
		f.write(str(cost) + '\t' + str(cost_noiso) + '\t' + str(cost_iso) + '\t'
		 + str(normalized_cost) + '\t' + str(normalized_cost_noiso) + '\t' 
		 + str(normalized_cost_iso) + '\n')
		   
	return cost

def get_opt_args():
	# Fabbri et al. 2017 optimised parameter (Table A1)
	# g Kur		   μS			   1.539e-4					   0.9895–1.633 × 10 −4
	# P CaT		   nA m M −1	   0.04132					  0.04132–0.05191
	# P CaL		   nA m M −1	   0.4578					  0.3475–0.5262
	# V dL		   mV			   -16.45					  −20.97 to −13.73
	# k dL		   mV			   4.337					  3.892–4.401
	# I NaK,max	   nA			   0.08105					  0.0623–0.1063
	# K NaCa	   nA			   3.343					  2.733–4.964
	# K up		   n M			   286.1					  204.6–356.2
	# k s		   s −1			   1.480e8					 1.460–1.847 × 10 8
	# τ difCa	   s			   5.469e-5					 5.469–7.951 × 10 −5
	# kf CM		   m M s −1		   1.642e6					 1.164–1.839 × 10 6
	# kf CQ		   m M s −1		   175.4					  155.2–194.6

	# optimised parameters from Fabbri et al., note that they are
	# different set than in Loewe et al. 2019 --> commented out
	# value are according to Loewe model
	initial = dict()
	initial['GKur']		 = 1.539e-4
	initial['P_CaT']	 = 0.04132
	initial['P_CaL']	 = 0.4578
	initial['V_dL']		 = -16.45
	initial['k_dL']		 = 4.337
	initial['INaK_max']	 = 0.08105
	initial['K_NaCa']	 = 3.343
	initial['K_up']		 = 286.1
	initial['k_s']		 = 1.480e8
	initial['tau_difCa'] = 5.469e-5
	initial['kf_CM']	 = 1.642e6
	initial['kf_CQ']	 = 175.4

	# in comments: are set in Loewe.model (values from Fabbri)
	#initial['INaK_max']		  = 0.137171 #0.08105
	initial['P_CaL']		  = 0.5046812 #0.4578
	initial['k_dL']			  = 5.854 #4.337
	initial['V_dL']			  = -7.7713 #-16.45085327
	#initial['k_fL']		   = 5.1737 # 5.3
	initial['k_fL']			  = 1.19870 # optimized value
	#initial['V_fL']		   = -12.1847 #-37.4
	#initial['V_fL']		   = -17.5 # new initial value starting at the top of the range
	initial['V_fL']			  = -21.9277 # optimized value
	initial['GKur']			  = 0.00007062 #0.0001539
	initial['Gto']			  = 0.00167348 #0.0035
	initial['GKr_']			  = 0.00499 #0.00424
	initial['GKs_']			  = 0.000863714 #0.00065
	initial['Gf_K']			  = 0.0117 #0.00268
	initial['Gf_Na']		  = 0.00696 #0.00159
	initial['EC50_SK']		  = 0.7 #0.7
	initial['n_SK']			  = 2.2 #2.2
	initial['GSK']			  = 0.000165 #0
	initial['dynamic_Ki_Nai'] = 1 #0
	
	# initial optimized parameters
	initial['GKur']		 = 4.24715857e-05
	initial['P_CaL']	 = 3.71787309e-01
	initial['V_dL']		 = -1.18839932e+01
	initial['k_dL']		 = 5.41492018e+00
	initial['INaK_max']	 = 1.01834688e-01
	initial['k_fL']			  = 4.97966543e+00
	initial['V_fL']		   = -3.49433557e+01


	# initial ranges -- TODO: change to more meaningfull values
	constrains = dict()
	constrains['k_fL']	   = (0.0, 8.75) # experimental range = [0.0, 8.75]
	#constrains['k_fL']		= (0, 1e3)
	constrains['V_fL']	   = (-35.0, -16.5) # experimental range = [-16.5, -35.0]
	#constrains['V_fL']		= (-1e3, 1e3)
	constrains['Gto']	   = (0, 1e3)
	constrains['GKr_']	   = (0, 1e3)
	constrains['GKs_']	   = (0, 1e3)
	constrains['Gf_K']	   = (0, 1e3)
	constrains['Gf_Na']	   = (0, 1e3)
	constrains['EC50_SK']  = (0, 1e3)
	constrains['n_SK']	   = (0, 1e3)
	constrains['GSK']	   = (0, 1e3)
	constrains['dynamic_Ki_Nai']	  = (0, 1)

	# Fabbri et al. 2017 based the constrains on the method from Sakar, Sobie 2010
	# widen the possible constraints, as the Loewe model already contains initial
	# values outside of these ranges
	constrains['GKur']		 = (0.9895e-5, 1.633e-4) #((0.9895e-4, 1.633e-4)
	constrains['P_CaT']		 = (0.04132, 0.05191)
	constrains['P_CaL']		 = (0.3475, 0.5262)
	constrains['V_dL']		 = (-21.0, -1.0) #(-20.97, -13.73)
	constrains['k_dL']		 = (2.0, 10.0) #(3.892, 4.401)
	#constrains['INaK_max']	  = (0.01, 0.25) #(0.0623, 0.1063)
	constrains['INaK_max']	 = (0.0623, 0.1063)
	constrains['K_NaCa']	 = (2.733, 4.964)
	constrains['K_up']		 = (204.6, 356.2)
	constrains['k_s']		 = (1.460e8, 1.847e8)
	constrains['tau_difCa']	 = (5.469e5, 7.951e5)
	constrains['kf_CM']		 = (1.164e6, 1.839e6)
	constrains['kf_CQ']		 = (155.2, 194.6)

	# Fabbri et al. 2017 page 2392:
	# We set weight i = 2 for MDP, CL and CL prolongation
	# induced by Cs + , whereas weight i was set to 1 for all other
	# features.
	weights = dict()
	weights['vm_min']		= 1#1.5
	weights['CL']			= 4#2
	weights['Maxdvdt']		= 1#1
	weights['APD20']		= 0.5#0.5
	weights['APD50']		= 0.5#0.5
	weights['APD90']		= 1.0#0.75
	weights['vm_overshoot'] = 1.5#0.75
	weights['DDR100']		= 1#1.5
	weights['ca_min']		= 0.75#1
	weights['ca_overshoot'] = 0.75#1
	weights['TA']			= 0.5#1
	weights['TD20']			= 0.5#1
	weights['TD50']			= 0.5#1
	weights['TD90']			= 0.5#1
	
	# added weight vector for isoprenaline optimization
	
	weights_iso = dict()
	weights_iso['vm_min']		= 1#1#2
	weights_iso['CL']			= 4#5#6
	weights_iso['Maxdvdt']		= 0#0#1
	weights_iso['APD20']		= 0#0#0.5
	weights_iso['APD50']		= 0#0.5
	weights_iso['APD90']		= 0#0#0.5
	weights_iso['vm_overshoot'] = 1.5#1#0.5
	weights_iso['DDR100']		= 0#0#0.5
	weights_iso['ca_min']		= 0#0#0.5
	weights_iso['ca_overshoot'] = 0#0#0.5
	weights_iso['TA']			= 0#0#0.5
	weights_iso['TD20']			= 0#0#0.5
	weights_iso['TD50']			= 0#0#0.5
	weights_iso['TD90']			= 0#0#0.5
	

	# Fabbri et al. 2017 Table 5
	# MDP			   mV		   −61.7	  6.1
	# CL			   ms		   828		  21
	# (dV/dt) max	   V s −1	   4.6		  1.7
	# APD 20		   ms		   64.9		  23.9
	# APD 50		   ms		   101.5	  38.2
	# APD 90		   ms		   143.5	  49.3
	# OS			   mV		   16.4		  0.9
	# DDR 100		   mV s −1	   48.9		  25.5

	# Fabbri et al. 2017 Table 6 from Verkerk et al. 2013
	# Ca i range	  n M		 105–220
	# TA			  n M		 115
	# TD 20			  ms		 138.9
	# TD 50			  ms		 217.4
	# TD 90			  ms		 394.0

	target = dict()
	target['vm_min']	   = -61.7
	target['CL']		   = 828
	target['Maxdvdt']	   = 4.6
	target['APD20']		   = 64.9
	target['APD50']		   = 101.5
	target['APD90']		   = 143.5
	target['vm_overshoot'] = 16.4
	target['DDR100']	   = 48.9

	target['ca_min']	   = 105
	target['ca_overshoot'] = 220
	target['TA']		   = 115
	target['TD20']		   = 138.9
	target['TD50']		   = 217.4
	target['TD90']		   = 394.0
	
	# Targets for simulated Fabbri model using 1mm of iso
	
	# 	target_iso = dict()
# 	target_iso['vm_min']	   = -57.46 # using Fabbri model output
# 	target_iso['CL']		   = 635 # using Fabbri model output
# 	target_iso['Maxdvdt']	   = 8.5 # +60% from non iso target
# 	target_iso['APD20']		   = 60.0 # -10% from non iso target
# 	target_iso['APD50']		   = 90.0 # -10% from non iso target
# 	target_iso['APD90']		   = 125.0 # -10% from non iso target
# 	target_iso['vm_overshoot'] = 16.0 # slightly lower than non iso target
# 	target_iso['DDR100']	   = 52.12# using Fabbri model output
# 
# 	target_iso['ca_min']	   = 120.0 # +15% on non iso target
# 	target_iso['ca_overshoot'] = 250.0 # +20% on non iso target
# 	target_iso['TA']		   = 130.0 # +15% on non iso target
# 	target_iso['TD20']		   = 120.0 # -5% on non iso target
# 	target_iso['TD50']		   = 200.0 # -10% on non iso target
# 	target_iso['TD90']		   = 350.0 # -15% on non iso target
	
	target_iso = dict()
	target_iso['vm_min']	   = -57.46 #-61.7
	target_iso['CL']		   = 635 #530 # about 55% increase over non iso #635 #828
	target_iso['Maxdvdt']	   = 11.64 #?? 4.6
	target_iso['APD20']		   = 75 #64.9
	target_iso['APD50']		   = 119 #101.5# experimental value
	target_iso['APD90']		   = 152 #143.5
	target_iso['vm_overshoot'] = 16.0 #28.34 #16.4# experimental change
	target_iso['DDR100']	   = 52.12#?? 48.9

	target_iso['ca_min']	   = 98.23 #105
	target_iso['ca_overshoot'] = 222.0 #?? 220
	target_iso['TA']		   = 123.77 #?? 115
	target_iso['TD20']		   = 134.0 #138.9
	target_iso['TD50']		   = 188.0 #217.4
	target_iso['TD90']		   = 459.0 #394.0
	
	# Fabbri et al. 2017 claims to use SEM for the computations, here
	# is the difference explained:
	# https://doi.org/10.1177/0253717620933419
	# TODO: should we compute SEM or rather update the cost function?
	# here n = 3
	n_experiments = 3
	
	SD = dict()
	SD['vm_min']	   = 6.1
	SD['CL']		   = 21
	SD['Maxdvdt']	   = 1.7 #0.5 #1.7
	SD['APD20']		   = 23.9 #6.5 #23.9
	SD['APD50']		   = 38.2 #10 #38.2
	SD['APD90']		   = 49.3 #15 #49.3
	SD['vm_overshoot'] = 0.9
	SD['DDR100']	   = 25.5 #5 #25.5

	# Fabbri et al. 2017 page 2392:
	# Because the experimental data on the calcium transient
	# are from only one cell, we arbitrarily set the SEM of each of
	# the calcium transient features to 40% in our optimization
	# procedure.

	# NOTE: not sure if this multiplication does really make sense as
	# it allows for quite a large range that do not have any effects
	# on the cost
	
	# TODO include sd_factor in weights
	sd_factor = 0.2
	
	SD['ca_min']	   = sd_factor*105#0.4*105
	SD['ca_overshoot'] = sd_factor*220#0.4*220
	SD['TA']		   = sd_factor*115#0.4*115
	SD['TD20']		   = sd_factor*138.9#0.4*138.9
	SD['TD50']		   = sd_factor*217.4#0.4*217.4
	SD['TD90']		   = sd_factor*394.0#0.4*394.0
	
	# Calculating SEM from SD
	# SEM = SD / sqrt(n)
	
	SEM = dict()
	
	# for n=1 SEM = SD
	# calcium transient features based on only one cell
	SEM = SD
	sqrt_n = n_experiments **(0.5)
	SEM['vm_min']	   		= SD['vm_min'] / (sqrt_n)
	SEM['CL']		   		= SD['CL'] / (sqrt_n)
	SEM['Maxdvdt']	   		= SD['Maxdvdt'] / (sqrt_n)
	SEM['APD20']		   	= SD['APD20'] / (sqrt_n)
	SEM['APD50']		  	= SD['APD50'] / (sqrt_n)
	SEM['APD90']		  	= SD['APD90'] / (sqrt_n)
	SEM['vm_overshoot'] 	= SD['vm_overshoot'] / (sqrt_n)
	SEM['DDR100']	   		= SD['DDR100'] / (sqrt_n)
	
	

	# labels as in parse_parameters(parameters)
	labels = ['INaK_max', 'P_CaL', 'k_dL', 'V_dL', 'k_fL', 'V_fL', 'GKur',
			  'Gto', 'GKr_', 'GKs_', 'Gf_K', 'Gf_Na', 'EC50_SK', 'n_SK', 
			  'GSK', 'dynamic_Ki_Nai', 'P_CaT', 'K_NaCa']

	# Note: lists needs to be filled in the order the labels are ordered
	optimize = ['INaK_max', 'P_CaL', 'k_dL', 'V_dL', 'k_fL', 'V_fL', 'GKur', 'P_CaT', 'K_NaCa']

	return target, SD, weights, constrains, initial, labels, optimize, target_iso, weights_iso, SEM

def run():
	cost_args		   = get_opt_args()
	constrains		   = cost_args[3]
	initial			   = cost_args[4]
	labels			   = cost_args[5]
	optimize_variables = cost_args[6]
	xopt_array = []
	
	single_optimization_run = 1 # optimize once or in a loop
	number_of_optimization_runs = 10
	ftol = 0.005 # tolerance for convergence
	max_iterations = 250
	max_func_eval = 500

	bounds = []
	for key in optimize_variables:
		bounds.append((constrains[key][0], constrains[key][1]))
		
	x = []
	for label in labels:
		if label in optimize_variables:
			x.append(initial[label])
	
	# runs a 
	if (single_optimization_run == 1):
		# this will take initial simplex as a random points on elipsoid
		# with the center at initial and height of the difference between
		# the constrains edges
		initial_simplex = [[] for _ in range(len(optimize_variables) + 1)]
		for vortex in initial_simplex:
			point = dict()
			norm  = dict()
			for label in optimize_variables:
				norm[label] = random()
				point[label] = norm[label]*( constrains[label][1] - constrains[label][0]) + constrains[label][0]
				vortex.append(point[label])

		options = {'initial_simplex': initial_simplex, 'adaptive': False, 'maxiter': max_iterations, 'maxfev': max_func_eval}
		xopt = scipy.optimize.minimize(fun=cost_function, x0=x, args=cost_args, method='Nelder-Mead', bounds=bounds, options=options, tol=ftol)

		print(xopt)
	
	elif (single_optimization_run == 2):
		for i in range(number_of_optimization_runs):
			# this will take initial simplex as a random points on elipsoid
			# with the center at initial and height of the difference between
			# the constrains edges
			initial_simplex = [[] for _ in range(len(optimize_variables) + 1)]
			for vortex in initial_simplex:
				point = dict()
				norm  = dict()
				for label in optimize_variables:
					norm[label] = random()
					point[label] = norm[label]*( constrains[label][1] - constrains[label][0]) + constrains[label][0]
					vortex.append(point[label])

			options = {'initial_simplex': initial_simplex,'adaptive': False, 'maxiter': max_iterations, 'maxfev': max_func_eval}
			xopt = scipy.optimize.minimize(fun=cost_function, x0=x, args=cost_args, method='Nelder-Mead', bounds=bounds, options=options, tol=ftol)
			xopt_array.append(xopt)			
		print(xopt_array)
	
	# this branch takes a specific starting point x as input for the Nelder-Mead method
	# the method creates the initial simplex itself
	else:
		for i in range(2):
			j = 0
			for key in optimize_variables:
				x = []
				for label in labels:
					if label in optimize_variables:
						x.append(initial[label])
				x[j] = constrains[key][i]
				options = {'adaptive': False, 'maxiter': max_iterations, 'maxfev': max_func_eval} #, 'initial_simplex': initial_simplex}
				xopt = scipy.optimize.minimize(fun=cost_function, x0=x, args=cost_args, method='Nelder-Mead', bounds=bounds, options=options, tol=ftol)
				xopt_array.append(xopt)
				j = j + 1			
		print(xopt_array)	
		
if __name__ == '__main__':
	run()

# SPDX-FileCopyrightText: 2024 Karlsruhe Institute of Technology (KIT) <tomas.stary@kit.edu>
#
# SPDX-License-Identifier: GPL-3.0-or-later

CARPUTILS_BUILD=CPU

DURATION=100000
START_OUTPUT=70000
TIMESTEP_OUTPUT=1.0

#'INaK_max', 'P_CaL', 'k_dL', 'V_dL', 'k_fL', 'V_fL', 'GKur', 'Gto', 'GKr_', 'GKs_', 'Gf_K', 'Gf_Na', 'EC50_SK', 'n_SK', 'GSK', 'dynamic_Ki_Nai', 'P_CaT', 'K_NaCa', 'GKACh'
DAT = dat/metricsLoewe_0.123343_0.541663_4.06541_-21.2739_4.82457_-42.4654_0.000146257_0.00344552_0.0043752_0.000347724_0.00334638_0.00183503_0.7_2.2_8.44825e-05_1.0_0.0293517_2.53669_0.00287_dur$(DURATION)ms.dat

all: $(DAT) 

dat/Loewe_%ms.dat: run.py
	python3 $< --build $(CARPUTILS_BUILD) \
	--parameters $(word 1, $(subst _dur, ,$*)) \
	--start-out $(START_OUTPUT) \
	--imp-par ,Cao=1.8 \
	--dt-out $(TIMESTEP_OUTPUT) \
	--iks 1 \
	--ical 1 \
	--silent \
	--duration  $(word 2, $(subst _dur, ,$*)) --output $@

dat/metrics%.dat: compute_metrics.py dat/%.dat
	python3 $+ > $@

dat/costs%.dat: compute_cost.py dat/metrics%.dat
	python3 $+ > $@

.PRECIOUS: dat/metrics%.dat dat/Loewe_%ms.dat


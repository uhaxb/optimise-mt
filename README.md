<!--
SPDX-FileCopyrightText: 2024 Karlsruhe Institute of Technology (KIT) <tomas.stary@kit.edu>

SPDX-License-Identifier: CC0-1.0
-->

# Optimise

[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/reuse/api)](https://api.reuse.software/info/git.fsfe.org/reuse/api)

This repository contains simulation code extracted from "dialysis"

# Start it

To start clone the repository and run `docker-compose up`. Then connect in a way as specified in the output of that command.

NOTE: Currently the opencarp image does not provide required python3.9> package. Hence we are building our own image locally.

If you are at IBT, mount beauolais volume and import the image into docker as
```
docker load < beaujolais/Benutzer/ts110/openCARP-ubuntu22.04-dialysis-c425605d5.tar
```
this should import `docker_opencarp:latest`.

# Miscellaneous

This software uses [semantic versioning](https://semver.org/spec/v2.0.0.html).

This software [keeps a Changelog](https://keepachangelog.com/en/1.0.0/).

This software is a free software [licensed under GPLv3](./LICENSE).

#!/usr/bin/bash

# SPDX-FileCopyrightText: 2024 Karlsruhe Institute of Technology (KIT) <tomas.stary@kit.edu>
#
# SPDX-License-Identifier: Apache-2.0

echo "Container starting..."
echo "Connect by: docker-compose exec -u \`id -n\` opencarp"

if ! [ -z $@ ];
then
    echo "Executing: $@"
    exec "$@"
fi

echo "Going to sleep forever."
sleep inf
